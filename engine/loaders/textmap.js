import merge from "../../utils/merge";
import Sprite from "../sprite";


function get(x, y, z, cube) {
  if (x >= 0 && x < cube[0][0].length &&
      y >= 0 && y < cube[0].length &&
      z >= 0 && z < cube.length) {
    return cube[z][y][x];
  }
  return null;
}

export default async function(data) {
  let cube = [];
  let layers = data.layers;
  if (data.map) {
    layers = [{
      name: data.map,
      asset: await this.resolveAsset(data.map),
      map: await (
        await fetch(
          await this.resolveAsset(data.map)
        )
      ).text(),
    }];
  } else if (! layers) {
    layers = [{
      map: data.map
    }];
  }
  for (let layer of layers) {
    let textmap = layer.map;
    if (typeof textmap == "string") {
      textmap = textmap.split(/\n/);
    }
    if (data.shape != "cube") {
      textmap = [textmap];
    }
    let width = textmap[0][0].length;
    let height = textmap[0].length;
    let depth = textmap.length;

    let x = 0, y = 0, z = 0;
    let sx = 0, sy = 0, sz = 0;
    let scale = data.scale || 1;

    if (data.position) {
      sx = data.position.x;
      sy = data.position.y;
      sz = data.position.z || 0;
    }

    let spriteGrid = [];
    for (let slice of textmap) {
      y = 0;
      for (let row of slice) {
        let sprites = [];
        x = 0
        for (let cell of row) {
          let blueprint = data.key[cell];
          if (blueprint || typeof cell == "object") {
            let results = null;
            if (! blueprint) {
              blueprint = data.key[cell.extends] || cell.extends;
            }
            if (typeof cell == "object" && blueprint) {
              cell.extends = blueprint;
              results = await this.load([merge(cell, {
                extends: blueprint,
                position: {
                  x: sx + (x - width / 2) * scale,
                  y: sy + (height / 2 - y) * scale,
                  z: sz + (z - depth / 2) * scale,
                }
              })]);
            } else {
              results = await this.load([{
                extends: blueprint,
                position: {
                  x: sx + (x - width / 2) * scale,
                  y: sy + (height / 2 - y) * scale,
                  z: sz + (z - depth / 2) * scale,
                }
              }]);
            }
            // console.log(sx, x);

            for (let item of results) {
              if (item instanceof Sprite) {
                item.data.textmapCoords = {x, y, z};
                item.data.textmapLayer = layer.name || data.layer;
                sprites.push(item);
              }
            }
          } else {
            // console.log("not creating", x, y, z, cell, blueprint);
            sprites.push(null);
          }
          x += 1;
        }
        y += 1;
        spriteGrid.push(sprites);
      }
      z += 1;
      cube.push(spriteGrid);
    }
  }


  for (let layer of cube) {
    for (let row of layer) {
      for (let sprite of row) {
        if (sprite) {
          let {x, y, z} = sprite.data.textmapCoords;
          let neighbor_array = [
            ["ne", get(x - 1, y - 1, z, cube)],
            ["nw", get(x + 1, y - 1, z, cube)],
            ["se", get(x - 1, y + 1, z, cube)],
            ["sw", get(x + 1, y + 1, z, cube)],
            ["n", get(x + 0, y - 1, z, cube)],
            ["e", get(x - 1, y - 0, z, cube)],
            ["w", get(x + 1, y - 0, z, cube)],
            ["s", get(x + 0, y + 1, z, cube)],
            ["u", get(x, y, z + 1, cube)],
            ["d", get(x, y, z - 1, cube)],
          ];
          let neighbors = {}
          for (let [d, s] of neighbor_array) {
            if (s) {
              neighbors[d] = s;
            }
          }
          sprite.trigger("mapping", {x, y, z, neighbors});
          sprite.data.neighbors = neighbors;
        }
      }
    }
  }

  for (let layer of cube) {
    for (let row of layer) {
      for (let sprite of row) {
        if (sprite) {
          sprite.trigger("mapped");
        }
      }
    }
  }
}
