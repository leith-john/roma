import {
  AnimationMixer,
  Group,
  SkeletonHelper,
} from "three";

import BVHLoader from "./BVHLoader.js";

export default function (data) {
  return new Promise(resolve => {

    let loader = new BVHLoader();

    let source = this.resolveAsset(`static/animation/${data.name}.bvh`);
    loader.load(source, ( result ) => {

      // let skeletonHelper = new SkeletonHelper(result.skeleton.bones[0]);
      // skeletonHelper.skeleton = result.skeleton;
      // this._skeleton = result.skeleton;

      let skeletonHelper = new SkeletonHelper(this._avatar.skeleton.bones[0]);
      skeletonHelper.skeleton = this._avatar.skeleton;

      // this._avatar.bind(result.skeleton);

      var boneContainer = new Group();
      boneContainer.scale.set(0.1, 0.1, 0.1);
      boneContainer.add(result.skeleton.bones[ 0 ]);

      console.log("bvh loaded", result);

      this.scene.add( skeletonHelper );
      this.scene.add( boneContainer );

      // play animation
      let mixer = new AnimationMixer( skeletonHelper );
      mixer.clipAction( result.clip ).setEffectiveWeight( 1.0 ).play();

      let render = () => {
        mixer.update(30 / 1000);
        setTimeout(render, 30);
      };
      render();

      this.clip = result.clip;
      console.log("clip", this, this.clip, result);

      resolve();
    });
  });
};