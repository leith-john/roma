from __future__ import print_function

import json
import os
import time
import datetime

import requests
from PIL import Image
from invoke import run, task

android_key = dict(
    location=".",
    build_location="./platforms/android/build/outputs/apk/",
    keystore_path="./keys/android.keystore",
    keystore_password="42pass",
    key_password="42pass",
    dist_location="./dist/app.apk",
    alias="mg",
    slug="mg",
)


class Struct:
    def __init__(self, **entries):
        self.__dict__.update(entries)


def local(cmd, shell=None):
    print(cmd)
    run(cmd, shell=shell)


@task()
def serve(ctx):
    local("npm run dev")


@task()
def build(ctx, app=False):
    if app:
        local("NODE_ENV=app npm run build")
    else:
        local("npm run build")


@task()
def local_android(ctx):
    local("NODE_ENV=app LOCAL=1 npm run build")
    local("cordova run --device")


@task()
def build_android(ctx):
    build(ctx, app=True)
    local((
        """
cordova build android --release -- \
              --keystore='{keystore_path}' \
              --alias='parade' \
              --storePassword='{keystore_password}' \
              --password='{key_password}'
cp platforms/android/build/outputs/apk/android-release.apk dist/
    """.format(**android_key)))


@task()
def upload_android(ctx, track="beta"):
    local("google-play-publisher -t {} -a api.json dist/android-release.apk".format(
        track
    ))


@task()
def killstart_ios(ctx):
    local("cordova platforms remove ios")
    local("cordova platforms add ios")


@task()
def download(
        ctx,
        code,
        server="https://parade.velocitywebworks.com",
        download=True):
    """ Download settings/media for a parade client """

    shared = False
    if code == "smart":
        shared = True

    url = "{server}/parade/profile/?code={code}&dt={time}".format(
        server=server,
        code=code,
        time=time.time()
    )
    print("accessing: {}".format(url))
    response = requests.get(url, verify=True)

    if response.ok:
        client = response.json()

        app_settings = dict(SERVER=server, CODE=code, SHARED=shared)

        # create settings file
        try:
            os.makedirs("src/clients/{}/config".format(code))
            with open("src/clients/{}/config/index.js".format(code), "w") as fh:
                fh.write("""
/**
 * Options:
 * ad_duration: duration an ad shows before rotating (milliseconds)
 */
export default {\n};
""")
        except OSError:
            pass

        # save globals file
        app = Struct(**android_key)
        generated_filename = os.path.join(
            app.location, "build/generated.js")
        with open(generated_filename, "w") as fileh:
            fileh.write('module.exports = {};'.format(
                json.dumps(app_settings)))

        # update app name
        source_filename = os.path.join(app.location, "config-sample.xml")
        dest_filename = os.path.join(app.location, "config.xml")
        with open(source_filename, "r") as filer:

            # the first app we made had an different app id than the rest
            app_id = "com.velocitywebworks.{app}.{code}".format(
                code=code,
                app=app.slug
            )

            android_app_id = client['app'].get("android") or app_id
            ios_app_id = client['app'].get("ios") or app_id
            version = datetime.datetime.now().strftime("%Y.%m.%d")
            app_name = client['app'].get("name") or code

            content = filer.read()
            content = content.format(
                app_name=app_name,
                app_id=app_id,
                android_app_id=android_app_id,
                ios_app_id=ios_app_id,
                version=version,
                datestamp=datetime.date.today().strftime("%Y%m%d")
            )

            with open(dest_filename, "w") as filew:
                filew.write(content)

        # update media
        if download:
            # TODO: splash
            # local("cp default-splash.png {}/splash.png".format(app.location))
            default_splash = True
            for media in client["media"]:
                print(media)
                purpose = media['purpose']
                if purpose == "splash":
                    filename = ["splash.png"]
                    default_splash = False
                elif purpose == "icon":
                    filename = ["icon.png"]
                elif purpose == "logo":
                    filename = ["src/assets/logo.png"]
                else:
                    filename = None

                if filename:
                    source = requests.get(
                        media['file'], stream=True, verify=True)

                    filename = os.path.join(app.location, *filename)
                    with open(filename, 'wb') as fd:
                        for chunk in source.iter_content(1024):
                            if chunk:
                                fd.write(chunk)

                    try:
                        if not media['file'].endswith(".png"):
                            im = Image.open(filename)
                            source = "%s.png" % filename.rsplit(".", 1)[0]
                            im.save(filename)
                            print("downloading %s for %s" % (filename, purpose))
                    except IOError:
                        print("failed to use %s for %s" % (filename, purpose))

            try:
                local("convert icon.png -resize 512 icon512.png")
                local("npx cordova-icon".format(app.location))
                local("npx cordova-splash".format(app.location))
                local("cp splash.png src/assets/splash.png")
            except IOError as ex:
                import traceback
                traceback.print_exc()
                print("Failed to prepare images", ex)
                print(""" if you get a message about cordova-splash not being found just install it `npm install -g cordova-splash`""")
    else:
        print("\n\n\n *** Invalid Parade (Server responded with {}) *** \n\n".format(response.status_code))


@task()
def push(ctx, to_, message, title=None, url=None, noteid=None):
    topics = []

    topics.append("/topics/" + to_)

    package = {
        "notification": {
            "title": title,
            "body": message,
            "content_available": True,
            "icon": "icon_push",
            "color": "#ff1493",
        },
        "data": {
            "title": title,
            "message": message,
            # "sound": sound,
            "url": url,
            # "cmd": data.get("command"),
        }
    }
    if noteid:
        package['notification']["tag"] = noteid

    for topic in topics:
        package['to'] = topic
        response = requests.post(
            url="https://gcm-http.googleapis.com/gcm/send",
            headers={
                "Authorization": "key=AIzaSyCcubOtsQDecDIxeAphRq69O69GvrzW4xY",
                "Content-Type": "application/json",
            },
            json=package
        )
        print(topic, response.status_code, response.text)
