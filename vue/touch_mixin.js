import {Vector3} from "three";
import nudged from "nudged";

const PI4 = Math.PI / 4;

export default {
  data() {
    return {
      position: null,
      angle: 0,
      floor: null,
      startZoom: 1,
      zoom: 1,
      start: {},
      lastInputs: {},
      keys: {},
      touchCount: 0,
    }
  },
  mounted() {
    this._key_listener = (e) => this.handleKeyboard(e);
    document.addEventListener("keydown", this._key_listener, false);
    document.addEventListener("keyup", this._key_listener, false);
  },
  destroyed() {
    document.removeEventListener("keydown", this._key_listener);
    document.removeEventListener("keyup", this._key_listener);
  },
  methods: {
    handleKeyboard(event) {
      this.keys[event.key.toLowerCase()] = event.type === 'keydown';
    },
    async handleInput(event) {
      let box = this.$el.getBoundingClientRect();

      // normalize input
      let inputs = {};
      if (event.type == "touchcancel") {
        this.touchCount = 0;
      } else if (event.type == "touchend") {
        if (this.touchCount === 1) {
          inputs = this.lastInputs;
          for (let key in inputs) {
            inputs[key].tap = true;
          }
        }
        // only reset when all fingers are removed
        if (event.touches.length === 0) {
          this.touchCount = 0;
        }
      } else if (event.touches) {
        event.preventDefault();
        for (let touch of event.touches) {
          inputs[touch.identifier] = {
            x: touch.pageX - box.width / 2,
            y: touch.pageY - box.height / 2,
            event: event.type,
            tap: (event.type == "touchstop") ? 1 : 0,
            btns: (event.type !== "touchcancel" || event.type !== "touchstop") ? 1 : 0
          };
        }
        this.touchCount = Math.max(
          this.touchCount,
          event.touches.length
        );
      } else {
        inputs[event.pointerId || "m"] = {
          x: event.pageX - box.width / 2,
          y: event.pageY - box.height / 2,
          event: event.type,
          tap: (event.type == "click" || event.type == "mouseup" || event.type == "pointerup") ? 1 : 0,
          btns: event.buttons
        };
      }

      // clear starts
      if (Object.keys(this.start).length != Object.keys(inputs).length) {
        this.start = {};
        this.lastInputs = {};
      }

      // record starts
      let count = 0;
      for (let key in inputs) {
        let input = inputs[key];
        if (input.tap || input.btns >= 1) {
          count += 1;
          if (!this.start[key]) {
            this.start[key] = input;
          }
        } else {
          this.start[key] = undefined;
        }
      }

      // touchend doesn't give any touches
      // so we need to record the last placement
      if (Object.keys(inputs).length === 1) {
        this.lastInputs = inputs;
      }

      if (count >= 1) {
        if (this.keys.control && this.keys.alt || count >= 3) {
          // three fingers or ctrl + alt
          this.transform(inputs);
          this.touchCount = 2;
        } else if (this.keys.shift || this.keys.control || this.keys.alt || count === 2) {
          // two fingers or keyboard modifier
          this.transform(inputs);
          this.touchCount = 2;
        } else if (count === 1 && (event.pointerType === "pen" || ! this.penOnly)) {
          // one finer + penonly modifier
          let input = Object.values(inputs)[0];
          if (input.tap) {
            input.x += box.width / 2;
            input.y += box.height / 2;
            await this.handleAction(input);
          }
        }
      }
      if (count === 0 || event.type == "touchend") {
        this.endTransform();
        this.lastInputs = {};
        this.start = {};
      }
    },
    transform(inputs) {

      if (! this.position) {
        this.position = this.game.camera.position.clone();
        this.floor = this.game.cameraWidth;
        this.startZoom = 1;
        this.angle = this.game.camera.rotation.z;
      }
      let a = [];
      let b = [];
      for (let key in this.start) {
        if (inputs[key]) {
          a.push([this.start[key].x, this.start[key].y]);
          b.push([inputs[key].x, inputs[key].y]);
        }
      }
      if (a.length > 0) {
        let estimator = "" +
          (this.keys.shift ? "T" : "") +
          (this.keys.control ? "S" : "") +
          (this.keys.alt ? "R" : "") +
        "";
        let pivot = estimator == "R" ? [this.position.x, this.position.y] : undefined;
        // console.error("estimator", estimator || "TSR", a, b, pivot);
        let rot = nudged.estimate(estimator || "TSR", a, b, pivot);
        let trans = rot.getTranslation();

        // 1.5: offset of 45 degrees
        if (this.game.camera.rotation.x != 0) {
          trans = new Vector3(trans[0], trans[1] * 1.5);
        } else {
          trans = new Vector3(trans[0], trans[1]);
        }

        // snap to 90 degree angles
        let angle = this.angle + rot.getRotation();
        if (angle > Math.PI * 2) { angle = angle % (Math.PI * 2); }
        if (angle < 0) { angle = Math.PI * 2 + angle; }

        let tolerance = 0.25;
        if (Math.abs((angle + tolerance / 2) % PI4) < tolerance) {
          angle = Math.floor((angle + tolerance / 2) / PI4) * PI4;
        }

        this.game.camera.rotation.z = angle;

        trans.applyAxisAngle(new Vector3(0, 0, -1), angle);

        trans.x = -trans.x;
        trans.y = trans.y;
        trans.multiplyScalar(this.game.cameraWidth / this.game.width);
        trans.add(this.position);
        trans.z = this.game.camera.position.z;

        this.zoom = Math.min(Math.max(this.startZoom / rot.getScale(), 0.4), 1.6);

        this.game.camera.position.copy(trans);
        this.game.setCameraWidth(this.floor * this.zoom);

        let focus = trans.clone();
        if (this.game.camera.rotation.x != 0) {
          // i just guessed this number too for 45 degree ortho views

          // let offset = this.game.cameraWidth * 3.5 / this.zoom;
          // focus.add(new Vector3(0, offset, 0).applyAxisAngle(new Vector3(0, 0, -1), -this.angle));
          // focus.add(new Vector3(0, -offset, 0).applyAxisAngle(new Vector3(0, 0, -1), -angle));

          let offset = 30;

          let rot = focus.clone().add(
            new Vector3(0, offset, 0).applyAxisAngle(new Vector3(0, 0, -1), -this.angle)
          );

          focus.sub(rot);
          focus.applyAxisAngle(new Vector3(0, 0, -1), this.angle - angle);
          focus.add(rot);
        }
        this.game.camera.position.copy(focus);
      }
    },
    endTransform() {
      this.startZoom = this.zoom;
      this.start = {};
      this.angle = this.game.camera.rotation.z;
      this.position = null;
    }
  }
}