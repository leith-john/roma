import {Manipulation} from "./base.js";
import {
  Vector2,
  Vector3,
} from "three";
import nudged from "nudged";
import { PI2, PI4 } from "../utils/constants.js";


let SNAP = 30;

export class Transform extends Manipulation {

  get name() { return "transform"; }
  get cursor() { return "pointer"; }

  manipulate(board, inputs) {
    let group = board.layerGroup;
    if (group === board.canvas) {
      return;
    }
    if (! group.scale) {
      group.scale = 1;
    }
    if (! group.start) {
      group.start = group.position.clone();
      group.trans = new Vector3();
    }
    let a = [];
    let b = [];
    for (let key in board.startInputs) {
      if (board.inputs[key]) {
        let a2 = this.project(board, board.startInputs[key].x, board.startInputs[key].y);
        a.push([a2.x, a2.y]);
        let b2 = this.project(board, board.inputs[key].x, board.inputs[key].y);
        b.push([b2.x, b2.y]);
      }
    }

    if (a.length > 0) {

      let estimator = (
        (board.keys.shift ? "T" : "") +
        // (board.keys.control ? "S" : "") +
        (board.keys.alt ? "R" : "")
      ) || "TR";

      let world = new Vector3(group.x, group.y, 0);
      group.mesh.parent.localToWorld(world);

      let rot = nudged.estimate(estimator, a, b, [world.x, world.y]);
      let trans = rot.getTranslation();
      trans = new Vector3(trans[0], trans[1]);

      if (/R/.test(estimator)) {
        // find angle
        let angle = group.lastAngle + rot.getRotation();
        if (angle < 0) {
          angle = Math.PI * 2 + angle;
        }

        // snap to 90 degree angles
        let tolerance = 0.15;
        if (Math.abs((angle + tolerance/2) % PI4) < tolerance) {
          angle = Math.floor((angle + tolerance / 2) / PI4) * PI4;
        }

        group.angle = angle;
      }

      if (/T/.test(estimator)) {
        let plocal = new Vector3();

        // create a local vector
        group.mesh.parent.worldToLocal(plocal);
        group.mesh.parent.worldToLocal(trans);
        trans = trans.sub(plocal);

        group.lastScale = Math.min(Math.max(group.scale * rot.getScale(), 0.25), 4.0);
        group.trans = trans;
        group.x = group.start.x + trans.x * group.lastScale;
        group.y = group.start.y + trans.y * group.lastScale;
        // group.mesh.scale.x = group.lastScale;
        // group.mesh.scale.y = group.lastScale;
      }

      this.clean(
        group.position,
        board.canvas.width - group.width,
        board.canvas.height - group.height
      );
    }
  }

  project(board, x, y) {
    let point = new Vector3(
      (x / board.width) * 2,
      (y / board.height) * 2,
      5
    ).unproject( board.camera );

    // direction to point from camera
    let dir = point.sub(board.camera.position).normalize();
    let distance = - (board.camera.position.z) / dir.z;

    point = board.camera.position.clone().add(
      dir.multiplyScalar(distance)
    );

    return point;
  }

  clean(p, w, h) {
    p.x = Math.max(
      -w / 2,
      Math.min(
        w / 2,
        p.x
      )
    );
    p.y = Math.max(
      -h / 2,
      Math.min(
        h / 2,
        p.y
      )
    );
  }

  end(board) {
    let group = board.layerGroup;
    if (group.start && group.trans) {
      group.scale = group.lastScale;
      group.lastAngle = group.mesh.rotation.z;
      board.$store.commit("layerUpdate", {
        id: group.id,
        position: {
          x: group.start.x + group.trans.x * group.scale,
          y: group.start.y + group.trans.y * group.scale,
        },
        angle: group.mesh.rotation.z
      });
      board.requestDraw();
      group.start = null;
      group.trans = null;
      group.lastScale = null;
    }
  }

}