import merge from "../utils/merge";


export default class Mod {
  get defaults() {
    return {};
  }

  constructor(game, data) {
    this.game = game;
    this.data = merge(this.defaults, data);
  }

  static async ready() {
    if (!this.loading) {
      this.loading = [];
    }
    await Promise.all(this.loading);
    this.loading.length = 0;
    return;
  }

  static registerModule(name) {
    this.modules[name] = this;
  }

  static async registerBehavior(name, klass) {
    if (!this.loading) {
      this.loading = [];
    }
    this.loading.push(new Promise(async r => {
      klass = await klass;
      if (klass.default) {
        klass = klass.default;
      }
      this.modules[name] = klass;
      r();
    }));
  }

  static hasModule(name) {
    return this.modules[name] !== undefined;
  }
}

Mod.modules = {};
