/* globals document */
import Mod from "./base";
import GamePadSource from "./inputs/gamepad.js";
import MouseSource from "./inputs/mouse.js";
import KeyboardSource from "./inputs/keyboard.js";
import KeyMouseSource from "./inputs/keymouse.js";
import TouchSource from "./inputs/touch.js";


/**
 * Finds and sync players
 * Disconnects broken players
 * sends normalized commands to player sprites
 */
export default class Players extends Mod {

  get defaults() {
    return {
      sources: ["touch", "keymouse", "gamepad"], // "keyboard", "keymouse", "network"]
      controls: {
        default: {},
      },
    };
  }

  constructor(game, data) {
    super(game, data);
    game.players = [];
    game.playerSources = [];
    game.controls = data.controls;

    let sources = {
      touch: TouchSource,
      mouse: MouseSource,
      keyboard: KeyboardSource,
      keymouse: KeyMouseSource,
      gamepad: GamePadSource
    };

    for (let source of this.data.sources) {
      let klass = sources[source];
      if (klass) {
        game.playerSources.push(new klass(game));
      } else {
        console.error("invalid source", source, sources);
      }
    }
  }

  update() {
    // watch for new player sources
    for (let source of this.game.playerSources) {
      if (source.isUsed()) {
        if (source.actor && source.actor.alive) {
          source.update();
        } else {
          source.unregister();
          source.actor = null;
        }
      }
    }
  }

  destroy() {
    for (let source of this.game.playerSources) {
      source.destroy();
    }
  }
}


Players.registerModule("players");
Players.registerBehavior("controller", import("../behaviors/controller.js"));
Players.registerBehavior("dangerous", import("../behaviors/dangerous.js"));
Players.registerBehavior("deathspawn", import("../behaviors/deathspawn.js"));
Players.registerBehavior("expire", import("../behaviors/expire.js"));
Players.registerBehavior("goto_cursor", import("../behaviors/goto_cursor.js"));
Players.registerBehavior("goto_other", import("../behaviors/goto_other.js"));
Players.registerBehavior("health", import("../behaviors/health.js"));
Players.registerBehavior("spawn", import("../behaviors/spawn.js"));
Players.registerBehavior("weapon", import("../behaviors/weapon.js"));
Players.registerBehavior("wire/sensor", import("../behaviors/wire/sensor.js"));
Players.registerBehavior("wire/wire", import("../behaviors/wire/wire.js"));
