/* globals THREE */
import Mod from "./base";
import {Vector3} from "three";

export default class PlayersFocus extends Mod {

  get defaults() {
    return {
      group: 1,
      y_offset: 0,
    };
  }

  update() {
    // center camera on players
    let count = 0;
    let center = new Vector3();
    for (let sprite of this.game.sprites.filter(s => s.player)) {
      center.add(sprite.position);
      count += 1;
    }
    if (count > 0) {
      center.multiplyScalar(1/count);
      if (this.data.xmin) { center.x = Math.max(this.data.xmin, center.x); }
      if (this.data.xmax) { center.x = Math.min(this.data.xmax, center.x); }
      if (this.data.ymin) { center.y = Math.max(this.data.ymin, center.y); }
      if (this.data.ymax) { center.y = Math.min(this.data.ymax, center.y); }

      this.game.camera.position.x = center.x;
      this.game.camera.position.y = center.y - this.data.y_offset;
      this.game.camera.lookAt(new Vector3(
        center.x, center.y, 0
      ));
    }
  }
}


PlayersFocus.registerModule("players_focus");
