/* globals window THREE */
import deepmerge from "deepmerge";
import { Euler, Quaternion } from "three";
import { GameObjectDef } from "../engine/defs.js";
import Mod from "./base";

export interface PhysicsObjectDef extends GameObjectDef {
  type: "module";
  name: "physics";
  gravity: [number, number];
}

class PromDate {
  constructor() {
    this.ready = new Promise((resolve, reject) => {
      this.resolve = resolve;
      this.reject = reject;
    });
  }
}

// import Behavior from "./base";

export default class Physics extends Mod {
  constructor(game, data) {
    super(game, data);
    this.ready = new PromDate();
    this.bodyId = 0;
    this.jointId = 0;
    this.sprites = {};
    this.joints = {};
    this.game = game;
    game.world = this;

    // let klass = null;
  }

  async load() {
    await this.loadWorker();
    await this.ready.ready;
  }

  async loadWorker() {
    this.worker = null;
    if (data.engine == "oimo") {
      this.worker = new Worker(
        new URL("../workers/oimo_worker.js?worker", import.meta.url),
      );
      // klass = require("worker-loader!../workers/oimo_worker.js");
      this.includeZ = true;
    } else {
      const MyWorker = await import("../workers/planck_worker.js?worker");
      console.log("worker", MyWorker);
      this.worker = new MyWorker();
      // klass = require("worker-loader!../workers/planck_worker.js");
      this.includeZ = false;
    }

    // this.worker = klass();
    // this.worker = new Worker("../workers/planck_worker.js");
    this.worker.postMessage({ event: "start", data: data });
    this.worker.onmessage = (e) => {
      // console.log("msg");
      let buffer = new DataView(e.data);
      let mode = buffer.getUint32(0);
      // console.log("msg", mode);
      if (mode === 1) {
        this.updateBodies(buffer);
        this.ready.resolve();
      } else if (mode === 2) {
        setTimeout(() => {
          this.updateBodyProperties(buffer);
        });
      } else if (mode === 3) {
        this.ready.resolve();
      }
    };
    this.worker.onerror = (e) => {
      console.error(e);
    };
  }

  updateBodyProperties(buffer) {
    let id = buffer.getUint32(1 * 4);
    let sprite = this.sprites[id];
    if (sprite) {
      sprite.body.mass = buffer.getFloat32(2 * 4);
      sprite.body.inertia = buffer.getFloat32(3 * 4);
      sprite.body.resolve(id);
      sprite.body.resolve = null;
    } else {
      console.error("mesh not found", id, buffer);
    }
  }

  updateBodies(buffer) {
    let values = 15;
    let jvalues = 7;
    let count = buffer.getUint32(1 * 4);
    let joints = buffer.getUint32(2 * 4);
    let x = 3;

    // console.log("updating", count, "bodies")
    for (; count > 0; x += values) {
      count -= 1;
      let id = buffer.getUint32((x + 0) * 4);
      let ax = buffer.getFloat32((x + 1) * 4);
      let ay = buffer.getFloat32((x + 2) * 4);
      let az = buffer.getFloat32((x + 3) * 4);
      let aw = buffer.getFloat32((x + 4) * 4);
      let dax = buffer.getFloat32((x + 5) * 4);
      let day = buffer.getFloat32((x + 6) * 4);
      let daz = buffer.getFloat32((x + 7) * 4);
      let px = buffer.getFloat32((x + 8) * 4);
      let py = buffer.getFloat32((x + 9) * 4);
      let pz = buffer.getFloat32((x + 10) * 4);
      let dx = buffer.getFloat32((x + 11) * 4);
      let dy = buffer.getFloat32((x + 12) * 4);
      let dz = buffer.getFloat32((x + 13) * 4);
      let contacts = buffer.getUint32((x + 14) * 4);

      // TODO: only update active
      // skip inactives
      let sprite = this.sprites[id];
      if (sprite) {
        // console.log("update", az, px, py);
        // sprite.timeline.add(this.game.time, this.game.time + this.game.timeScale, {
        //   angle: angle,
        //   position: new THREE.Vector3(px, py)
        // });

        sprite.orientation = new Quaternion(ax, ay, az, aw);

        let euler = new Euler().setFromQuaternion(sprite.orientation);
        sprite.angle = euler.z;
        sprite.position.x = px;
        sprite.position.y = py;
        if (this.includeZ) {
          sprite.position.z = pz;
        }
        // console.log("rigidbody", sprite.body.id, sprite.angle, euler, euler.z);
        // console.log("contacts", sprite.body.id, contacts);
        if (sprite.body) {
          sprite.body.torque = sprite.body.nextTorque || 0;
          sprite.body.angularVelocity = daz;
          // TODO: do we need this or just for planck?
          // sprite.body.angularVelocity = (daz + sprite.body.torque) / 30;
          sprite.body.nextTorque = 0;
          sprite.body.velocity.x = dx;
          sprite.body.velocity.y = dy;
          sprite.body.velocity.z = dz;
          sprite.body.contacts = contacts;
        } else {
          console.error("missing body", id, sprite.body);
        }
      } else {
        console.log("no matching sprite 2");
      }
    }

    for (; joints > 0; x += jvalues) {
      joints -= 1;
      let id = buffer.getUint32((x + 0) * 4);
      let ax = buffer.getFloat32((x + 1) * 4);
      let ay = buffer.getFloat32((x + 2) * 4);
      let bx = buffer.getFloat32((x + 3) * 4);
      let by = buffer.getFloat32((x + 4) * 4);
      let ra = buffer.getFloat32((x + 5) * 4);
      let imp = buffer.getFloat32((x + 6) * 4);

      let sprite = this.joints[id];
      if (sprite) {
        // console.log("update", angle, px, py);
        // sprite.timeline.add(this.game.time, this.game.time + this.game.timeScale, {
        //   angle: ra,
        //   position: new THREE.Vector3(ax, ay)
        // });
        // sprite.angle = ra;
        // sprite.position.x = ax;
        // sprite.position.y = ay;
        // TODO: set other joint props
      } else {
        // console.log("no matching sprite 1");
        // this.worker.postMessage({ event: "destroyJoint", data: {id}});
      }
    }
  }

  impulseBody(data) {
    let sprite = this.sprites[data.id];
    if (sprite) {
      if (data.torque) {
        sprite.body.nextTorque = data.torque;

        // TODO: do we need this? or just for planck?
        // data.torque *= sprite.body.inertia * 30;
      }
      this.worker.postMessage({ event: "impulseBody", data });
    }
  }

  changeBody(data) {
    let sprite = this.sprites[data.id];
    if (sprite) {
      this.worker.postMessage({ event: "changeBody", data });
    }
  }

  changeJoint(data) {
    let sprite = this.joints[data.id];
    if (sprite) {
      this.worker.postMessage({ event: "changeJoint", data });
    }
  }

  createBody(sprite, data) {
    return new Promise((resolve, reject) => {
      let id = ++this.bodyId;
      data = deepmerge({ id: id }, data);
      data.position = [sprite.position.x, sprite.position.y, sprite.position.z];
      data.force = sprite.data.force;
      data.angle = sprite.angle;
      data.group = sprite.data.group;

      // console.log("body", data);
      sprite.body.resolve = resolve;
      this.sprites[id] = sprite;

      this.worker.postMessage({ event: "createBody", data: data });
    });
  }

  referenceSprite(sprite, label) {
    if (label == "self") {
      return sprite.body.id;
    }

    if (typeof label == "number") {
      return label;
    }

    let a = this.game.sprites.find((s) => s.data.name == label);
    if (a && a.body) {
      return a.body.id;
    }
    return null;
  }

  async createJoint(sprite, data) {
    let id = ++this.jointId;
    data.id = id;
    if (!data.position) {
      data.position = { x: sprite.position.x, y: sprite.position.y };
    }
    data.angle = sprite.angle;

    let a = await this.referenceSprite(sprite, data.a);
    let b = await this.referenceSprite(sprite, data.b);

    if (!a || !b) {
      console.error(
        `additional error data: a is "${data.a}" / b is "${data.b}"`,
      );
      console.log(`more error data: sprite a is "${a}" / sprite b is "${b}"`);
      throw new Error("Invalid sprite for joint a/b");
    }
    data.a = a;
    data.b = b;
    this.joints[id] = sprite;

    this.worker.postMessage({ event: "createJoint", data });
  }

  updateBody(data) {
    this.worker.postMessage({ event: "updateBody", data });
  }

  destroyBody(sprite) {
    if (sprite.body && sprite.body.id in this.sprites) {
      delete this.sprites[sprite.body.id];
      this.worker.postMessage({
        event: "destroyBody",
        data: { id: sprite.body.id },
      });
    } else if (sprite.body) {
      console.error("couldn't delete", sprite.body.id);
    }
  }

  destroyJoint(sprite) {
    if (sprite.joint && sprite.joint.id in this.joints) {
      delete this.joints[sprite.joint.id];
      this.worker.postMessage({
        event: "destroyJoint",
        data: { id: sprite.joint.id },
      });
    } else if (sprite.joint) {
      console.error("couldn't delete", sprite.joint.id);
    }
  }

  async preUpdate() {
    this.ready = new PromDate();
    if (this.worker) {
      this.worker.postMessage({ event: "update" });
      // console.log("waiting");
    }
    await this.ready.ready;
  }

  update() {}

  setGravity(data) {
    console.log("gravity", data);
    this.worker.postMessage({ event: "setGravity", data });
  }

  destroy() {
    // TODO:
    this.worker.terminate();
  }
}

Physics.registerModule("physics");
Physics.registerBehavior(
  "physics/rigidbody",
  import("../behaviors/physics/rigidbody.js"),
);
Physics.registerBehavior(
  "physics/joint",
  import("../behaviors/physics/joint.js"),
);
Physics.registerBehavior(
  "physics/gravity",
  import("../behaviors/physics/gravity.js"),
);
