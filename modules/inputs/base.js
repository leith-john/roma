
export default class BaseSource {

  get name() { return "unknown"; }

  constructor(game) {
    this.game = game;
    this.actor = null;
    this.actions = null;
    this._ready = 0;  // user has interacted with this device
    this.timeout = 5000;
    this.readyAt = 0;
  }

  /** user indicated they are using this device, but not registered */
  isReady() {
    return this.isAvailable() && this._ready > Date.now() - 10000 && Date.now() > this.readyAt;
  }

  /** this is not being used by someone else */
  isAvailable() {
    return (! this.pending && ! this.isUsed()) && this.isCapable();
  }

  /** this is available on this platform */
  isCapable() {
    throw new Error("isCapable is not implemented");
  }

  isUsed() {
    return this.actor ? true : false;
  }

  register(actor, controller="default") {
    console.log("register", controller, this.game.controls);
    actor.player = true;
    actor.data.name = this.name;
    this.pending = false;
    this.actor = actor;
    this.actions = this.game.controls[controller];
    this.times = {};
    this.keys = {};
    this.actor.states.actions = {};
  }

  update() {
    throw new Error();
  }

  unregister() {
    this.readyAt = Date.now() + this.timeout;
  }

  destroy() {
    this.unregister();
  }
}