/* globals: THREE */
import BaseSource from "./mouse";
import Keyboard from "./keys";
import {Vector3} from "three";

export default class KeyMouseSource extends BaseSource {
  get name() { return "Keyboard & Mouse"; }

  constructor(game) {
    super(game);
    if (this.isCapable()) {
      this.keyboard = new Keyboard();
    }
  }

  destroy() {
    super.destroy();
    this.keyboard.destroy();
  }

  update() {
    if (this.actor && this.keys.mouse) {
      let state = this.keyboard.getState();
      let mouse = this.keys.mouse;
      let point = new Vector3(
        (mouse.x / this.game.width) * 2 - 1,
        - (mouse.y / this.game.height) * 2 + 1,
        5
      ).unproject( this.game.camera );

      // direction to point from camera
      let dir = point.sub(this.game.camera.position).normalize();
      let distance = - (this.game.camera.position.z) / dir.z;

      point = this.game.camera.position.clone().add(
        dir.multiplyScalar(distance)
      );
      this.actor.states.cursor = point;
      this.actor.states.vector = new Vector3();

      for (let action in this.actions) {
        let value = 0;

        for (let key of this.actions[action]) {
          let negation = 1;

          // TODO: optimize, precache this somewhere
          if (/^-/.test(key)) {
            negation = -1;
            key = key.slice(1);
          }
          if (this.keys[key]) {
            value = this.keys[key] * negation;
            break;
          }
          if (state[key]) {
            value = state[key] * negation;
            break;
          }
        }

        if (action == "cursor.x" || action == "cursor.y") {
          // on the mouse
        } else if (action == "vector.x") {
          this.actor.states.vector.x = value;
        } else if (action == "vector.y") {
          this.actor.states.vector.y = value;
        } else if (action == "vector.z") {
          this.actor.states.vector.z = value;
        } else {
          this.actor.states[action] = value;
        }
      }

      // console.log(this.actor.states.vector);
      // this.actor.states.vector.normalize();
    }
  }
}