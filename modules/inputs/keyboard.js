import BaseSource from "./base";
import Keyboard from "./keys";


export default class KeyboardSource extends BaseSource {
  get name() { return "keyboard"; }

  constructor(game) {
    super(game);
    this.keyboard = new Keyboard();
  }

  isReady() {
    return this.isAvailable() && this.keyboard.ready > Date.now() - 10000 && Date.now() > this.readyAt;
  }

  isCapable() {
    return true;
  }

  destroy() {
    super.destroy();
    this.keyboard.destroy();
  }

  update() {
    if (this.actor) {
      let state = this.keyboard.getState();
      let point = this.actor.position.clone();
      point.add(this.actor.control.forward.multiplyScalar(10));

      this.actor.states.cursor = point;
      this.actor.states.vector = new THREE.Vector3();

      for (let action in this.actions) {
        let value = 0;

        for (let key of this.actions[action]) {
          let negation = 1;

          // TODO: optimize, precache this somewhere
          if (/^-/.test(key)) {
            negation = -1;
            key = key.slice(1);
          }
          if (state[key]) {
            value = state[key] * negation;
            break;
          }
        }

        if (action == "cursor.x") {
          this.actor.states.cursor.x = value;
        } else if (action == "cursor.y") {
          this.actor.states.cursor.y = value;
        } else if (action == "vector.x") {
          this.actor.states.vector.x = value;
        } else if (action == "vector.y") {
          this.actor.states.vector.y = value;
        } else {
          this.actor.states[action] = value;
        }
      }
    }
  }
}