export default class Keyboard {

  constructor() {
    this.keys = {};
    this.times = {};
    this._listener = this.listener.bind(this);
    document.addEventListener("keydown", this._listener);
    document.addEventListener("keyup", this._listener);
  }

  get ready() {
    return this._ready;
  }

  listener(e) {
    const key = e.key.toLowerCase();
    if (e.type == "keydown" && ! this.keys[key]) {
      this._ready = Date.now();
      this.keys[key] = Date.now();
    }

    if (e.type == "keyup") {
      let prev = this.keys[key] || Date.now();
      delete this.keys[key];
      this.times[key] = Date.now() - prev;
    }
  }

  destroy() {
    document.removeEventListener("keydown", this.listener);
    document.removeEventListener("keyup", this.listener);
  }

  getState() {
    let now = Date.now();
    for (let key in this.keys) {
      let time = this.keys[key];
      if (time) {
        this.times[key] = now - time;
      }
    }

    let state = {};
    for (let key in this.times) {
      // setting 16 second activation period
      state[key] = Math.min(1, this.times[key] / 200);
    }
    this.times = {};

    // if (Object.keys(state).length) {
    //   console.log(state);
    // }

    return state;
  }
}