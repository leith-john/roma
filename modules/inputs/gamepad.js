import BaseSource from "./base";
import {Vector3} from "three";

function precisionRound(number, precision) {
  let factor = Math.pow(10, precision);
  return Math.round(number * factor) / factor;
}

export default class GamePadSource extends BaseSource {
  get name() { return "gamepad"; }

  constructor(game) {
    super(game);
    this.gamepad = null;
    this.calibrating = 5;
    this.calibration = [];

    if (this.isCapable()) {
      this.listenForGamepads();
    }
  }

  destroy() {
    window.removeEventListener("gamepadconnected", this._gamepadConnected);
    window.removeEventListener("gamepaddisconnected", this._gamepadDisconnected);
  }

  listenForGamepads() {
    if (! this._isListening) {
      this._isListening = true

      this._gamepadConnected = (e) => {
        if (this.gamepad === null) {
          this.gamepad = e.gamepad.index;
        }
        console.log("Gamepad connected at index %d: %s. %d buttons, %d axes.",
          e.gamepad.index, e.gamepad.id,
          e.gamepad.buttons.length, e.gamepad.axes.length);
      };
      this._gamepadDisconnected = (e) => {
        this.gamepad = null;
        console.log("Gamepad disconnected from index %d: %s",
          e.gamepad.index, e.gamepad.id);
      };

      window.addEventListener("gamepadconnected", this._gamepadConnected);
      window.addEventListener("gamepaddisconnected", this._gamepadDisconnected);
    }
  }

  isCapable() {
    // @ts-ignore
    return window.Gamepad ? true : false;
  }

  isAvailable() {
    let pads = navigator.getGamepads();
    for (let pad of pads) {
      if (pad) {
        if (this.gamepad === null) {
          console.log("pad already connected", pad.index);
          this.gamepad = pad.index;
        }
        for (let button of pad.buttons) {
          if (button.pressed) {
            this._ready = Date.now();
            break;
          }
        }
        break;
      }
    }
    return this.gamepad !== null && super.isAvailable();
  }

  update() {
    let gamepad = navigator.getGamepads()[this.gamepad];
    if (this.actor) {
      let now = Date.now();

      let idx = 0;
      for (let button of gamepad.buttons) {
        this.keys[`btn${idx}`] = button.value;
        idx += 1;
      }
      idx = 0;
      if (this.calibrating) {
        this.calibration = gamepad.axes.slice(0);
        this.calibrating -= 1;
      }
      for (let axes of gamepad.axes) {
        this.keys[`axis${idx}`] = axes - this.calibration[idx];
        idx += 1;
      }

      let delta = new Vector3();
      for (let action in this.actions) {
        let value = 0;

        for (let key of this.actions[action]) {
          let negation = 1;

          // TODO: optimize, precache this somewhere
          if (/^-/.test(key)) {
            negation = -1;
            key = key.slice(1);
          }
          if (this.keys[key]) {
            value = this.keys[key] * negation;
            break;
          }
        }

        if (action == "cursor.x") {
          delta.x = value * 10;
        } else if (action == "cursor.y") {
          delta.y = value * 10;
        } else {
          this.actor.states[action] = value;
        }
      }

      this.actor.states.cursor = this.actor.position.clone().add(delta);
    }
  }
}