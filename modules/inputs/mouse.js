/* globals: THREE */
import BaseSource from "./base";


export default class MouseSource extends BaseSource {
  get name() { return "Mouse"; }

  constructor(game) {
    super(game);
    if (this.isCapable()) {
      this._listener = this.listener.bind(this);
      this.keys = {};
      document.addEventListener("mousemove", this._listener, true);
      document.addEventListener("mouseup", this._listener, true);
      document.addEventListener("mousedown", this._listener, true);
      document.addEventListener("contextmenu", (e) => e.preventDefault(), true);
    }
  }

  listener(e) {
    if (e.type != "mousemove") {
      // e.preventDefault();
      this._ready = Date.now();
    }

    if (this.actor) {
      this.keys.mouse = {x: e.clientX, y: e.clientY};
      if (e.type == "mousedown") {
        this.keys["btn" + e.button] = true;
      }
      if (e.type == "mouseup") {
        this.keys["btn" + e.button] = false;
      }
    }
  }

  isCapable() {
    return true;
  }

  destroy() {
    super.destroy();
    document.removeEventListener("mousemove", this._listener);
    document.removeEventListener("mouseup", this._listener);
    document.removeEventListener("mousedown", this._listener);
    document.removeEventListener("contextmenu", this._listener);
  }

  update() {
    if (this.actor) {
      if (this.keys.mouse) {
        let mouse = this.keys.mouse;
        let point = new THREE.Vector3(
          (mouse.x / this.game.width) * 2 - 1,
          - (mouse.y / this.game.height) * 2 + 1,
          5
        ).unproject( this.game.camera );

        // direction to point from camera
        let dir = point.sub(this.game.camera.position).normalize();
        let distance = - (this.game.camera.position.z) / dir.z;

        point = this.game.camera.position.clone().add(
          dir.multiplyScalar(distance)
        );
        this.actor.states.cursor = point;

        for (let action in this.actions) {
          let value = 0;

          for (let key of this.actions[action]) {
            let negation = 1;

            // TODO: optimize, precache this somewhere
            if (/^-/.test(key)) {
              negation = -1;
              key = key.slice(1);
            }
            if (this.keys[key]) {
              value = this.keys[key] * negation;
              break;
            }
          }

          if (action == "cursor.x" || action == "cursor.y") {
            // already done
          } else {
            this.actor.states[action] = value;
          }
        }
      }
    }
  }
}