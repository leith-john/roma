import BaseSource from "./base";
import {Vector3} from "three";


export default class TouchSource extends BaseSource {
  get name() { return "Touch"; }

  constructor(game) {
    super(game);
    if (this.isCapable()) {
      this._listener = this.listener.bind(this);
      this.keys = {};
      document.addEventListener("touchmove", this._listener, true);
      document.addEventListener("touchcancel", this._listener, true);
      document.addEventListener("touchstart", this._listener, true);
      document.addEventListener("touchend", this._listener, true);
      document.addEventListener("contextmenu", (e) => e.preventDefault(), true);
    }
  }

  listener(e) {
    if (e.type != "touchmove") {
      // e.preventDefault();
      this._ready = Date.now();
    }

    if (this.actor) {
      if (e.targetTouches) {
        let x = 0;
        let y = 0;
        let count = 0;
        Array.prototype.forEach.call(e.targetTouches, (touch) => {
          x += touch.clientX;
          y += touch.clientY;
          count += 1;
        });
        if (count > 0) {
          this.keys.mouse = {x: x / count, y: y / count};
        }
      }

      if (e.type == "touchstart") {
        this.keys["btn" + e.button] = true;
      }
      if (e.type == "touchend" || e.type == "touchcancel") {
        this.keys["btn" + e.button] = false;
      }
    }
  }

  isCapable() {
    return true;
  }

  destroy() {
    super.destroy();
    document.removeEventListener("touchmove", this._listener);
    document.removeEventListener("touchup", this._listener);
    document.removeEventListener("touchdown", this._listener);
  }

  update() {
    if (this.actor) {
      if (this.keys.mouse) {
        let mouse = this.keys.mouse;
        let point = new Vector3(
          (mouse.x / this.game.width) * 2 - 1,
          - (mouse.y / this.game.height) * 2 + 1,
          this.actor.position.z
        ).unproject( this.game.camera );

        // direction to point from camera
        let dir = point.sub(this.game.camera.position).normalize();
        let distance = - (this.game.camera.position.z) / dir.z;

        point = this.game.camera.position.clone().add(
          dir.multiplyScalar(distance)
        );
        this.keys.projected = point;
        this.keys.mouse = null;
      }
      if (this.keys.projected) {
        let point = this.keys.projected;

        this.actor.states.cursor = point;
        this.actor.states.vector = point.clone().sub(this.actor.position).normalize();

        for (let action in this.actions) {
          let value = 0;

          for (let key of this.actions[action]) {
            let negation = 1;

            // TODO: optimize, precache this somewhere
            if (/^-/.test(key)) {
              negation = -1;
              key = key.slice(1);
            }
            if (this.keys[key]) {
              value = this.keys[key] * negation;
              break;
            }
          }

          if (action == "cursor.x" || action == "cursor.y") {
            // already done
          } else {
            this.actor.states[action] = value;
          }
        }
      }
    }
  }
}