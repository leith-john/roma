import Ammo from "ammo.js";
import {BaseProxy} from "./physics_worker.js";
import {Vector3} from "three";


const TS = 60;

let trans = new Ammo.btTransform(); // taking this out of the loop below us reduces the leaking

class Proxy extends BaseProxy {

  createWorld(gravity) {
    let collisionConfiguration = new Ammo.btDefaultCollisionConfiguration(),
        dispatcher             = new Ammo.btCollisionDispatcher(collisionConfiguration),
        overlappingPairCache   = new Ammo.btDbvtBroadphase(),
        solver                 = new Ammo.btSequentialImpulseConstraintSolver(),
        world                  = new Ammo.btDiscreteDynamicsWorld(dispatcher, overlappingPairCache, solver, collisionConfiguration);
    world.setGravity(new Ammo.btVector3(gravity[0], gravity[1], -9));
    return world;
  }

  step() {
    this.world.stepSimulation(1/TS, 10);
  }

  // add body
  createBodyProperties(shape) {
    let options = {
      type: {
        cube: "box",
        circle: "sphere",
        sphere: "sphere",
        cylinder: "cylinder",
      }[shape.type] || "box",
      rot: [0, 0, shape.angle],
      move: ! shape.static,
      density: shape.density || 0.1,
      friction: shape.friction || 0.1,
      autoSleep: true,
      linearDamping: shape.linear_damping || 0.1,
      angularDamping: shape.angular_damping || 0.0,
      // belongsTo: parseInt(shape.category, 2),
      // collidesWith: parseInt(shape.mask, 2),

      // fixedRotation: shape.fixed_rotation ? true : false,
      // bullet: shape.bullet
      // options.filterGroupIndex = shape.group * -1;
    };
    let btshape = null;
    if (options.type == "box") {
      btshape = new Ammo.btBoxShape(new Ammo.btVector3(shape.width / 2, shape.height / 2, shape.depth || 0.5));
    } else if (options.type == "sphere") {
      options.size = [shape.radius, shape.radius, shape.radius];
      btshape = new Ammo.btSphereShape(shape.radius);
    }
    // console.log("options", options, btshape);

    let mass = 10 * shape.density;
    let localInertia = new Ammo.btVector3(0, 0, 0);

    let transform = new Ammo.btTransform();
    transform.setIdentity();
    transform.setOrigin(new Ammo.btVector3(shape.position[0] || 1, shape.position[1] || 1, shape.position[2] || 0));
    if (!shape.static) {
      btshape.calculateLocalInertia(mass, localInertia);
    } else {
      mass = 0;
    }

    let myMotionState = new Ammo.btDefaultMotionState(transform);
    let rbInfo = new Ammo.btRigidBodyConstructionInfo(mass, myMotionState, btshape, localInertia);
    let body = new Ammo.btRigidBody(rbInfo);

    this.world.addRigidBody(body);
    body.getMotionState().getWorldTransform(trans)
    // console.log("body", body, shape);
    body.static = shape.static;
    return body;
  }

  createJoint(def) {
    console.error("create joint not implemented");
    // let klass = {
    //   "weld": planck.WeldJoint,
    //   "revolute": planck.RevoluteJoint,
    //   "distance": planck.DistanceJoint
    // }[def.type];
    // let joint = this.world.createJoint(klass(
    //   def,
    //   this.bodies[def.a],
    //   this.bodies[def.b],
    //   planck.Vec2(def.position[0], def.position[1])
    // ));
    // joint._id = def.id;
    // this.joints[def.id] = joint;
    return null;
  }

  getInitBodyProperties(body) {
    return {
      mass: body.mass,
      inertia: 1 / TS,
    }
  }

  updateBodyProperties(body, data) {
    console.log("update body properties");
    // TODO: if (data.angle !== undefined) body.rotation.set(0, 0, data.angle);
    if (data.x !== undefined && data.y !== undefined) {
      let state = body.getMotionState();
      console.log("motion state", state);
      body.position.set(data.x, data.y, data.z);
    }
  }

  destroyBodyProperties(body) {
    this.world.remove(body);
  }

  applyForce(body, force) {
    let i = 1; // TS;
    body.applyCentralForce(new Ammo.btVector3(force.x / i, force.y / i, 0));
    // console.log("mass", body);
    // body.position.set(body.position.x, body.position.y, 0);
  }

  applyTorque(body, torque) {
    // torque *= 1000;
    console.log("torque", torque, body);
    // console.log("body", body);
    body.applyTorqueImpulse(new Ammo.btVector3(0, 0, torque));
    // console.log("at", torque.toFixed(2), body);
  }

  changeBodyProperties(data) {
    console.log("anybody know how to change a body?");
  }

  getBodies() {
    return Object.values(this.bodies).filter(b => !b.static && b.isActive());
  }

  getJoints() {
    return [];
  }

  getBodyProperties(body) {
    body.getMotionState().getWorldTransform(trans);
    let p = trans.getOrigin();
    let r = trans.getRotation();
    let pv = body.getLinearVelocity();
    let rv = body.getAngularVelocity();
    console.log("state", rv.x(), rv.y(), rv.z());
    return {
      r: {x: r.x(), y: r.y(), z: r.z(), w: r.w()},
      rv: {x: rv.x(), y: rv.y(), z: rv.z()},
      p: {x: p.x(), y: p.y(), z: p.z()},
      pv: {x: pv.x(), y: pv.y(), z: pv.z()},
      contants: 0,
    }
  }

  getJointProperties(joint) {
    return {};
  }
}

let proxy = new Proxy();

onmessage = function (e) {
  e = e.data;
  if (!proxy[e.event]) {
    console.log("missing event", e.event);
  }
  proxy[e.event](e.data);
};
