
export class BaseProxy {

  createWorld(gravity) {
    throw new Error("Override createWorld");
  }
  setGravity(x, y, z=0) {
    throw new Error("Override setGravity");
  }
  createJoint(def) {
    throw new Error("Override createJoint");
  }
  changeJointProperties(body, data) {
    throw new Error("Override changeJointProperties");
  }
  changeBodyProperties(body, data) {
    throw new Error("Override changeBodyProperties");
  }
  createBodyProperties(shape) {
    throw new Error("Override createBodyProperties");
  }
  getInitBodyProperties(body) {
    throw new Error("Override getInitBodyProperties");
  }
  getBodyProperties(body) {
    throw new Error("Override getBodyProperties");
  }
  getJointProperties(body) {
    throw new Error("Override getJointProperties");
  }
  updateBodyProperities(body, data) {
    throw new Error("Override updateBodyProperities");
  }
  destroyBodyProperties(body) {
    throw new Error("Override destroyBodyProperties");
  }
  destroyJointProperties(joint) {
    throw new Error("Override destroyJointProperties");
  }
  applyForce(body, force) {
    throw new Error("Override applyForce");
  }
  applyTorque(body, torque) {
    throw new Error("Override applyTorque");
  }
  getBodies() {
    throw new Error("Override getBodies");
  }
  getJoints() {
    throw new Error("Override getJoints");
  }
  step() {
    throw new Error("Override step");
  }

  start(data) {
    let gravity = data.gravity || [0, -9.807];
    this.world = this.createWorld(gravity);
    this.bodies = {};
    this.joints = {};

    let buffer = new DataView(new ArrayBuffer(4));
    buffer.setUint32(0, 3);
    buffer = buffer.buffer;
    postMessage(buffer, [buffer]);
  }

  createBody(shape) {
    let body = this.createBodyProperties(shape);
    body._id = shape.id;
    this.bodies[shape.id] = body;
    this.impulseBody(shape);

    // not sure why i'm doing this
    let bd = this.getInitBodyProperties(body);
    let buffer = new DataView(new ArrayBuffer(4 + 3 * 4));
    buffer.setUint32(0, 2);
    buffer.setUint32(1 * 4, body._id);
    buffer.setFloat32(2 * 4, bd.mass);
    buffer.setFloat32(3 * 4, bd.inertia);
    buffer = buffer.buffer;
    postMessage(buffer, [buffer]);
  }

  updateBody(data) {
    let body = this.bodies[data.id];
    if (body) {
      // console.log("updating", body, data);
      this.updateBodyProperties(body, data);
    } else {
      console.error("body not found", data.id, typeof data.id);
    }
  }

  changeBody(data) {
    try {
      let body = this.bodies[data.id];
      if (body) {
        this.changeBodyProperties(body, data);
      } else {
        console.error("body not found", data.id, typeof data.id);
      }
    } catch(ex) {
      console.log(ex);
    }
  }

  changeJoint(data) {
    try {
      let joint = this.joints[data.id];
      if (joint) {
        this.changeJointProperties(joint, data);
      } else {
        console.error("joint not found", joint.id, typeof joint.id);
      }
    } catch(ex) {
      console.log(ex);
    }
  }

  destroyBody(data) {
    let body = this.bodies[data.id];
    if (body) {
      delete this.bodies[data.id];
      this.destroyBodyProperties(body);
    }
  }

  destroyJoint(data) {
    let joint = this.joints[data.id];
    if (joint) {
      delete this.joints[data.id];
      this.destroyJointProperties(joint);
    }
  }

  impulseBody(data) {
    try {
      let body = this.bodies[data.id];
      if (body) {
        if (data.force) {
          this.applyForce(body, data.force);
        }
        if (data.torque) {
          this.applyTorque(body, data.torque);
        }
      } else {
        console.error("body not found", data.id, typeof data.id);
      }
    } catch(ex) {
      console.log(ex);
    }
  }

  // run engine
  update() {
    this._running = true;
    let start = Date.now();
    // console.log("- starting update");

    try {
      // console.log("- update");
      this.step();
    } catch(ex) {
      console.log(ex);
    }

    let joints = this.getJoints();
    let bodies = this.getBodies();

    // bodies x bytes x values
    let values = 15;
    let jvalues = 7;
    let buffer = new DataView(new ArrayBuffer(
      3 * 4 + // metadata
      bodies.length * 4 * values + // bodies
      joints.length * 4 * jvalues // joints
    ));

    // type of data transfer is update
    buffer.setUint32(0 * 4, 1);

    // number of bodies in transfer
    buffer.setUint32(1 * 4, bodies.length);

    // number of joints in transfer
    buffer.setUint32(2 * 4, joints.length);

    let x = 3;
    for (let body of bodies) {
      let bd = this.getBodyProperties(body);
      buffer.setUint32((x + 0) * 4, body._id);
      buffer.setFloat32((x + 1) * 4, bd.r.x);
      buffer.setFloat32((x + 2) * 4, bd.r.y);
      buffer.setFloat32((x + 3) * 4, bd.r.z);
      buffer.setFloat32((x + 4) * 4, bd.r.w);
      buffer.setFloat32((x + 5) * 4, bd.rv.x);
      buffer.setFloat32((x + 6) * 4, bd.rv.y);
      buffer.setFloat32((x + 7) * 4, bd.rv.z);
      buffer.setFloat32((x + 8) * 4, bd.p.x);
      buffer.setFloat32((x + 9) * 4, bd.p.y);
      buffer.setFloat32((x + 10) * 4, bd.p.z || 0);
      buffer.setFloat32((x + 11) * 4, bd.pv.x);
      buffer.setFloat32((x + 12) * 4, bd.pv.y);
      buffer.setFloat32((x + 13) * 4, bd.pv.z || 0);
      // console.log("torque", (body.getAngularVelocity() / 30).toFixed(6));
      // console.log(`1: ${body._id}`, body.m_xf.q);
      // console.log(`mass: ${body.mass}`);
      // console.log("contacts", bd.contacts);
      let contactCount = body.didContact ? 1 : bd.contacts;
      body.didContact = false;
      buffer.setUint32((x + 14) * 4, parseInt(contactCount));

      x += values;
    }
    for (let joint of joints) {
      let jd = this.getJointProperties(joint);
      buffer.setUint32((x + 0) * 4, joint._id);
      buffer.setFloat32((x + 1) * 4, jd.ax);
      buffer.setFloat32((x + 2) * 4, jd.ay);
      buffer.setFloat32((x + 3) * 4, jd.bx);
      buffer.setFloat32((x + 4) * 4, jd.by);
      buffer.setFloat32((x + 5) * 4, jd.angle);
      buffer.setFloat32((x + 6) * 4, jd.impulse);
      x += jvalues;
    }
    buffer = buffer.buffer;
    postMessage(buffer, [buffer]);

    // for (let body of bodies) {
    //   console.log(`2: ${body._id} - ${body.getPosition()}`);
    //   // console.log(`mass: ${body.mass}`);
    //   // console.log(`inertia: ${body.inertia}`);
    // }
    // console.log("- update done", Date.now() - start);
    this._running = false;
  }

  // destroy
}
