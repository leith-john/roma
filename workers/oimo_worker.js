import {World} from "oimo";
import {BaseProxy} from "./physics_worker.js";
import {Vector3, Euler, Quaternion} from "three";


const TS = 30;

class OimoProxy extends BaseProxy {


  createWorld(gravity) {
    let world = new World({
      timestep: 1/TS,
      iterations: 10,
      broadphase: 3,
      worldScale: 0.1,
      gravity: [gravity[0], gravity[1], -9.50 * 20],
      // gravity: [0, 0, 0]
    });
    return world;
  }

  step() {
    this.world.step();
  }

  // add body
  createBodyProperties(shape) {
    let group = (shape.group ? "0000" : "1111") + (shape.category || "10000");
    let mask = "1111" + (shape.mask || "11111");
    if (shape.group) {
      group = group.substr(0, shape.group) + "1" + group.substr(shape.group + 1);
      mask = mask.substr(0, shape.group) + "0" + mask.substr(shape.group + 1);
    }
    // console.log(group, mask);
    let options = {
      type: {
        cube: "box",
        circle: "sphere",
        sphere: "sphere",
        cylinder: "cylinder",
      }[shape.type] || "box",
      pos: [shape.position[0], shape.position[1], shape.position[2] || 0],
      rot: [0, 0, shape.angle * (180/Math.PI)],
      move: ! shape.static,
      density: shape.density || 0.1,
      friction: shape.friction || 0.1,
      autoSleep: true,
      linearDamping: shape.linear_damping || 0.1,
      angularDamping: shape.angular_damping || 0.0,
      belongsTo: parseInt(group, 2),
      collidesWith: parseInt(mask, 2),
      // fixedRotation: shape.fixed_rotation ? true : false,
      // bullet: shape.bullet
      isTrigger: shape.isSensor ? true : false,
    };
    if (options.type == "box") {
      options.size = [shape.width || 1, shape.height || 1, shape.depth || 1];
    } else if (options.type == "sphere") {
      options.size = [shape.radius, shape.radius, shape.radius];
    }
    // console.log("options", options, options.size);
    let body = this.world.add(options);
    body.linear_damping = Math.max(0, Math.min(1, 1 - options.linearDamping));
    body.angular_damping = Math.max(0, Math.min(1, 1 - options.angularDamping));
    body.isSensor = shape.isSensor;
    // console.log("body", body);
    return body;
  }

  createJoint(def) {
    console.error("create joint not implemented");
    // let klass = {
    //   "weld": planck.WeldJoint,
    //   "revolute": planck.RevoluteJoint,
    //   "distance": planck.DistanceJoint
    // }[def.type];
    // let joint = this.world.createJoint(klass(
    //   def,
    //   this.bodies[def.a],
    //   this.bodies[def.b],
    //   planck.Vec2(def.position[0], def.position[1])
    // ));
    // joint._id = def.id;
    // this.joints[def.id] = joint;
    return null;
  }

  getInitBodyProperties(body) {
    return {
      mass: body.mass,
      inertia: 1 / TS,
    }
  }

  updateBodyProperties(body, data) {
    // TODO: if (data.angle !== undefined) body.rotation.set(0, 0, data.angle);
    if (data.x !== undefined && data.y !== undefined) {
      body.position.set(data.x, data.y, data.z);
    }
  }

  destroyBodyProperties(body) {
    this.world.remove(body);
  }

  applyForce(body, force) {
    let i = TS / (body.inverseMass * 20);
    // console.log(body);
    body.applyImpulse(body.position, new Vector3(force.x / i, force.y / i, (force.z || 0) / i));
    // body.position.set(body.position.x, body.position.y, 0);
  }

  applyTorque(body, torque) {
    // this is a 2d apply torque, so it stabilizes the other rotations
    torque = torque * TS;
    body.angularVelocity.z += torque;
    // let pos = body.position.clone();
    // pos.y += 1000;
    // body.applyImpulse(pos, new Vector3(torque, 0, 0));
  }

  changeBodyProperties(data) {
    console.log("anybody know how to change a body?");
  }

  getBodies() {
    return Object.values(this.bodies).filter(b => b.isDynamic || b.isSensor);
  }

  getJoints() {
    return [];
  }

  getBodyProperties(body) {

    let o = body.orientation;
    let orientation = new Quaternion(o.x, o.y, o.z, o.w);
    let euler = new Euler().setFromQuaternion(orientation);
    // let euler2 = new Euler(0, 0, euler.z);
    body.angularVelocity.x = body.angularVelocity.x * 0.1 - euler.x * 0.5 * TS;
    body.angularVelocity.y = body.angularVelocity.y * 0.1 - euler.y * 0.5 * TS;

    // apply damping
    body.linearVelocity.multiplyScalar(body.linear_damping);
    // body.angularVelocity.multiplyScalar(body.angular_damping);
    body.angularVelocity.z *= body.angular_damping;
    // console.log(body.angularVelocity.x, body.angularVelocity.y)

    // console.log("contacts", body.numContacts);
    return {
      r: body.orientation,
      rv: body.angularVelocity.clone().multiplyScalar(1/TS),
      p: body.position,
      pv: body.linearVelocity.clone().multiplyScalar(1/TS),
      contacts: body.numContacts,
    }
  }

  getJointProperties(joint) {
    return {};
  }
}

let proxy = new OimoProxy();

onmessage = function (e) {
  e = e.data;
  if (!proxy[e.event]) {
    console.log("missing event", e.event);
  }
  proxy[e.event](e.data);
};
