import { Color, ShaderMaterial, Texture } from "three";
import frag from "./blur.frag";
import vert from "./blur.vert";

// Define the shader material with blur effect
export function createBlurMaterial(
  map: Texture,
  color: Color,
  blur: number,
): ShaderMaterial {
  // Uniforms for the shader
  const uniforms = {
    u_texture: { value: map },
    u_color: { value: color },
    u_blurSize: { value: blur },
  };

  // Create and return the shader material
  const blurMaterial = new ShaderMaterial({
    uniforms: uniforms,
    vertexShader: vert,
    fragmentShader: frag,
    transparent: true,
  });

  return blurMaterial;
}
