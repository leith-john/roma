uniform sampler2D u_texture;
uniform int u_blurSize;
uniform vec3 u_color;

varying vec2 v_uv;

void main() {
    vec4 color = vec4(0.0, 0.0, 0.0, 0.0);

    float totalWeight = 0.0;

    // Gaussian weights: approximate with a simple distribution
    for (int x = -u_blurSize; x <= u_blurSize; x++) {
      for (int y = -u_blurSize; y <= u_blurSize; y++) {

        // reducing to pixel value
        // 100.0 came from cyoa2, might need to be adjusted
        vec2 offset = vec2(x, y) / 100.0;

        float distance = float(x * x + y * y);
        float weight = max(0.0, 1.0 - distance / float(u_blurSize));

        color += texture(u_texture, v_uv + offset) * weight;
        totalWeight += weight;
      }
    }

    // Normalize the color
    color /= totalWeight;
    color.rgb *= u_color;

    pc_fragColor = color;
}