import {
  BufferGeometry,
  Color,
  Float32BufferAttribute,
  LineBasicMaterial,
  LineSegments,
} from "three";

export class GridHelper extends LineSegments {
  constructor(
    width: number,
    height: number,
    wSegments: number,
    hSegments: number,
    color1: Color | string | number = 0x444444,
    color2: Color | string | number = 0x888888,
  ) {
    color1 = new Color(color1);
    color2 = new Color(color2);

    const sw = width / wSegments;
    const sh = height / hSegments;
    const hw = width / 2;
    const hh = height / 2;

    const vertices: number[] = [];
    const colors: number[] = [];

    let j = 0;
    for (let y = hh; y >= -hh; y -= sh) {
      for (let x = -hw; x < hw; x += sw) {
        if (x < hw) {
          vertices.push(x, y, 0, x + sw, y, 0);
          const color = y === 0 ? color1 : color2;
          color.toArray(colors, j);
          j += 3;
          color.toArray(colors, j);
          j += 3;
        }
        if (y > -hh) {
          vertices.push(x, y, 0, x, y - sh, 0);
          const color = x === 0 ? color1 : color2;
          color.toArray(colors, j);
          j += 3;
          color.toArray(colors, j);
          j += 3;
        }
      }
    }

    const geometry = new BufferGeometry();
    geometry.setAttribute("position", new Float32BufferAttribute(vertices, 3));
    geometry.setAttribute("color", new Float32BufferAttribute(colors, 3));

    const material = new LineBasicMaterial({
      vertexColors: true,
      toneMapped: false,
    });

    super(geometry, material);

    this.type = "GridHelper";
  }

  dispose() {
    this.geometry.dispose();
    this.material.dispose();
  }
}

if (import.meta.hot) {
  import.meta.hot.dispose((mod) => {
    console.log("dispose grid");
  });

  import.meta.hot.accept((mod) => {
    if (mod) {
      console.log("accept grid", mod);
    }
  });
}
