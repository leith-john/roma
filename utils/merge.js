import deepmerge from "deepmerge";


export default function merge(a, b) {
  let result = deepmerge(a, b, {arrayMerge: overwrite});
  return result;
};


function overwrite(destinationArray, sourceArray, options) {
  let objects = false;
  if (destinationArray.length) {
    objects |= typeof(destinationArray[0]) == "object";
  }
  if (sourceArray.length) {
    objects |= typeof(sourceArray[0]) == "object";
  }
  if (objects) {
    return destinationArray.concat(sourceArray);
  } else {
    return sourceArray;
  }
};
