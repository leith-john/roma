export interface CharRenderingOptions {
  size: number;
  font: string;
  shadow?: { color: string; blur: number };
}

export interface CharData {
  data: string;
  width: number;
  height: number;
}

export class CharRenderer {
  canvas: HTMLCanvasElement;
  measure: HTMLDivElement;
  maxSize = 240;

  constructor() {
    // canvas for drawing
    this.canvas = document.createElement("canvas");

    // create a div for measuring
    this.measure = document.createElement("div");
    this.measure.style.position = "absolute";
    this.measure.style.visibility = "hidden";
    this.measure.style.top = "0px";
    this.measure.style.left = "0px";
    this.measure.style.color = "white";
    this.measure.style.background = "black";
    this.measure.style.zIndex = "1000";
    this.measure.style.padding = "0px";
    this.measure.style.whiteSpace = "pre";
    this.measure.style.display = "block";

    document.body.appendChild(this.measure);
  }

  async renderCharacter(
    char: string,
    opts: CharRenderingOptions,
  ): Promise<CharData> {
    this.measure.style.fontSize = `${this.maxSize}px`;
    this.measure.style.fontFamily = opts.font;
    this.measure.innerHTML = char;
    getComputedStyle(this.measure);
    // await timer(3000);

    const box = this.measure.getBoundingClientRect();
    const width = box.width;
    const height = box.height;
    const scale = (opts.size || this.maxSize) / this.maxSize;

    this.canvas.style.background = "black";
    this.canvas.width = Math.floor(width);
    this.canvas.height = Math.floor(height);

    if (width === 0 || height === 0) {
      return { data: "", width: width * scale, height: height * scale };
    }

    // draw text on the box
    const ctx = this.canvas.getContext("2d");
    if (ctx && width > 0 && height > 0) {
      ctx.font = `${this.maxSize}px ${opts.font}`;

      // if (opts.shadow) {
      // ctx.shadowColor = "red";
      // ctx.shadowBlur = 30;
      // ctx.shadowOffsetX = 10;
      // ctx.shadowOffsetY = 10;
      // }
      ctx.lineWidth = 1;

      const ts = ctx.measureText(char);

      // ctx.fillStyle = "red";
      // ctx.fillRect(0, 0, width, height);
      // ctx.rect(0, 0, width, height);

      ctx.strokeStyle = "white";
      ctx.fillStyle = "white";
      ctx.fillText(char, 0, ts.fontBoundingBoxAscent);
      // ctx.strokeText(text, 0, ts.fontBoundingBoxAscent);
      // console.log("ts", ts, this.data);
      const url = this.canvas.toDataURL();
      // console.log("url", url);

      // render to an image
      return { data: url, width: width * scale, height: height * scale };
    }

    console.log("no context", width, height, char);
    throw new Error("MG: failed to open a rendering context");
  }
  dispose() {
    document.body.removeChild(this.measure);
  }
}
