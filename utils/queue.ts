export class FixedQueue<T = any> {
  size: number;
  array: (T | undefined)[];
  start: number;
  end: number;

  constructor(size: number) {
    this.size = size;
    this.array = new Array(size);
    this.start = 0;
    this.end = 0;
  }

  get length() {
    return this.end - this.start;
  }

  set length(v) {
    this.end = this.start + v;
  }

  get head() {
    return this.array[this.start % this.size];
  }

  shift() {
    if (this.start >= this.end) {
      return null;
    }
    return this.array[this.start++ % this.size];
  }

  push(item: T) {
    this.array[this.end++ % this.size] = item;
    if (this.end - this.start > this.size) {
      console.error("queue overflow", this.array);
      throw new Error(`Queue overflowed`);
    }
  }

  clear() {
    this.start = 0;
    this.end = 0;
  }
}

// export class Queue {
//   constructor(size) {
//     this.head = []
//   }
// }
