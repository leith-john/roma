import Behavior from "./base";
import ParticleEmitterBehavior from "./emitter";

export default class ParticleBehavior extends Behavior {

  get defaults() {
    return {
      group: "default"
    };
  }

  constructor(game, sprite, data) {
    super(game, sprite, data);

    this.group = ParticleEmitterBehavior.get(this.data.group);
    this.emitter = this.group.emit(sprite.position);
    this.updateEmitter();
    // console.log(this.emitter, this.emitter.alive);
  }

  updateEmitter() {
    this.emitter.position.value = this.sprite.position.clone();
    if (this.sprite.body.id) {
      this.emitter.angle.value = Math.PI / 2 - this.sprite.angle;
      this.emitter.velocity.value = this.sprite.body.velocity.clone().multiplyScalar(-0.00001);
    }
  }

  update() {
    // console.log("particles", this.sprite.mesh.rotation.z, this.sprite.angle );
    this.updateEmitter();

    if (! this.emitter.alive) {
      return false;
    }
    // return this.emitter.alive;
  }

  destroy() {
    this.group.group.releaseIntoPool(this.emitter);
  }
}
