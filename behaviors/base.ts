import type Game from "../engine/game.js";
import type Sprite from "../engine/sprite.js";
import Mod from "../modules/base.js";

export interface BehaviorData {
  name: string;
  enabled?: boolean;
}

export default class Behavior<T extends BehaviorData = BehaviorData> {
  game: Game;
  sprite: Sprite;
  name: string = "";
  data: T;
  listeners: Record<string, any> = {};
  postLoad?: Function;

  static register(name: string) {
    Mod.registerBehavior(name, this);
  }

  static hotDispose<T extends Behavior>(name: string) {
    const game: Game = window._hot_game;
    for (const sprite of game.sprites) {
      const behavior = sprite.getBehavior<T>(name);
      console.log("behavior", name, behavior);
      if (behavior) {
        behavior.destroy();
        console.log(`hot: removed ${name} from`, sprite.id);
      }
    }
  }

  static hotAccept<T extends Behavior>(name: string, mod: T) {
    const game: Game = window._hot_game;
    for (const sprite of game.sprites) {
      const behavior = sprite.getBehavior<T>(name);
      if (behavior) {
        const index = sprite.behaviors.indexOf(behavior);
        const newb = new mod(behavior.game, sprite, behavior.data);
        sprite.behaviors.splice(index, 1, newb);
        newb.load();
        console.log(`hot: updated ${name} from`, sprite.id);
      }
    }
  }

  get enabled() {
    return this.data.enabled;
  }

  set enabled(v) {
    this.data.enabled = v;
  }

  get defaults(): Partial<T> {
    return {
      enabled: true,
    } as Partial<T>;
  }

  constructor(game: Game, sprite: Sprite, data: T) {
    this.game = game;
    this.sprite = sprite;
    const cleanData = {};
    const uncleanData = game.resolveData(data);
    for (const key in uncleanData) {
      if (uncleanData[key] !== undefined) {
        cleanData[key] = uncleanData[key];
      }
    }
    this.data = { ...this.defaults, ...cleanData } as T;
  }

  async load() {
    return;
  }

  /**
   * @returns boolean
   */
  update(): boolean | void {
    return false;
  }

  destroy() {}

  trigger(event: string, data: Record<string, any>) {
    if (this.listeners[event]) {
      this.listeners[event](data);
    }
  }
}
