import Behavior from "./base";

export default class ExplosiveBehavior extends Behavior {

  get defaults() {
    return {
      after: 19
    };
  }

  constructor(game, sprite, data) {
    super(game, sprite, data);
    this.start = game.time;
  }

  update() {
    if (this.game.time - this.start > this.data.after && this.sprite.body.contacts > 0.001) {
      this.sprite.alive = false;
    }
  }
}
