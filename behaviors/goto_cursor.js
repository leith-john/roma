import Behavior from "./base";
import Controller from "./controller";
import {Vector3} from "three";

/**
 * The controller creates a cursor and this object tries to go there
 */
export default class GotoController extends Controller {

  constructor(game, sprite, data) {
    super(game, sprite, data);
    this.cooldown = 0;
    this.perfect_angle = data.perfect_angle;
  }

  turn(amount) {
    let sign = amount < 0 ? -1 : 1;
    let turn_rate = this.turnRate();
    this.game.world.impulseBody({
      id: this.sprite.body.id,
      torque: sign * Math.min(turn_rate, Math.abs(amount))
    });
  }

  accelerate(scale) {
    let delta = this.forward.multiplyScalar(this.data.speed * scale);
    this.game.world.impulseBody({
      id: this.sprite.body.id,
      force: {x: delta.x, y: delta.y},
    });
  }

  update() {
    // if (!this.agitated) {
    //   this.agitated = true;
    //   console.log("updating");
    //   this.game.world.impulseBody({
    //     id: this.sprite.body.id,
    //     // force: {x: delta.x, y: delta.y},
    //     torque: 1
    //   });
    // } else if (Math.abs(this.sprite.body.angularVelocity) > 0.001) {
    //   console.log("av", (-this.sprite.body.angularVelocity).toFixed(2));
    //   this.game.world.impulseBody({
    //     id: this.sprite.body.id,
    //     // force: {x: delta.x, y: delta.y},
    //     torque: -this.sprite.body.angularVelocity
    //   });
    // }
    let vel = this.sprite.body.velocity.clone();
    vel.z = 0;

    let cursor = this.sprite.states.cursor;
    let dist = 0
    if (cursor) {
      cursor = cursor.clone();
      cursor.z = this.sprite.position.z;
      dist = this.sprite.position.distanceTo(cursor);
    }
    if (cursor && dist > this.sprite.radius) {

      dist -= this.sprite.radius;
      let speed = this.sprite.getCharStat("speed") * this.data.speed;
      let mag = vel.length();
      let dot = cursor.clone().sub(this.sprite.position).dot(vel);
      let vector = this.sprite.states.vector.clone();
      if (dot < 0.0) {
        mag = -mag;
      }

      // slowdown before you get there? ver. 2
      // if (mag >= 0) {
      //   if (dist < mag) {
      //     console.log("slowing");
      //     speed = -Math.min(speed, mag - dist);
      //   } else if (dist / mag - 1 <= mag / (speed * 0.9)) {
      //     console.log("braking");
      //     speed = -speed;
      //   }
      //   if (speed > 0 && dist < speed * 2) {
      //     speed /= 2;
      //   }
      // } else {
      //   // vector.x = -vel.x;
      //   // vector.y = -vel.y;
      //   // console.log("slowly")
      //   speed /= 4;
      // }

      // slowdown before you get there ver. 1
      // let foo = (mag * mag) / (2 * speed);
      // if (mag > dist && mag - speed < dist) {
      //   console.log("almost");
      //   speed -= Math.min(speed, mag - dist);
      // } else if (mag + dist < speed) {
      //   console.log("slowly");
      //   speed = mag + dist;
      // } else if (dist < foo) {
      //   console.log("braking");
      //   speed = -speed;
      // }
      // speed *= 4.816955684007707

      let delta = null;
      if (vector) {
        delta = vector.multiply(new Vector3(
          speed,
          speed,
          1
        ));
        // console.log("delta", delta)
      } else {
        delta = this.forward.multiplyScalar(speed * (this.sprite.states.go || 0));
        delta.add(this.left.multiplyScalar(speed * (this.sprite.states.strafe || 0)));
      }

      let torque = this.turnTo(this.sprite.states.cursor);

      const impulse = {
        id: this.sprite.body.id,
        force: {x: delta.x, y: delta.y},
      }
      if (!this.perfect_angle) {
        impulse.torque = torque;
      }

      this.game.world.impulseBody(impulse);
      if (this.perfect_angle) {
        this.game.world.updateBody({
          id: this.sprite.body.id,
          angle: torque + this.sprite.angle,
        });
      }
    } else {
      // dampen
      this.game.world.impulseBody({
        id: this.sprite.body.id,
        // force: {x: -vel.x, y: -vel.y},
        torque: -this.sprite.body.angularVelocity * 0.1
      });
    }
  }
}
