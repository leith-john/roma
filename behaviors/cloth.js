/* globals window THREE */

import Behavior from "./base";

import {
  Vector3,
  Bone,
  Uint16BufferAttribute,
  Float32BufferAttribute,
  Skeleton,
  SkeletonHelper,
} from "three";

export var easings = {
  // no easing, no acceleration
  linear: function (t) { return t },
  // accelerating from zero velocity
  easeInQuad: function (t) { return t*t },
  // decelerating to zero velocity
  easeOutQuad: function (t) { return t*(2-t) },
  // acceleration until halfway, then deceleration
  easeInOutQuad: function (t) { return t<.5 ? 2*t*t : -1+(4-2*t)*t },
  // accelerating from zero velocity
  easeInCubic: function (t) { return t*t*t },
  // decelerating to zero velocity
  easeOutCubic: function (t) { return (--t)*t*t+1 },
  // acceleration until halfway, then deceleration
  easeInOutCubic: function (t) { return t<.5 ? 4*t*t*t : (t-1)*(2*t-2)*(2*t-2)+1 },
  // accelerating from zero velocity
  easeInQuart: function (t) { return t*t*t*t },
  // decelerating to zero velocity
  easeOutQuart: function (t) { return 1-(--t)*t*t*t },
  // acceleration until halfway, then deceleration
  easeInOutQuart: function (t) { return t<.5 ? 8*t*t*t*t : 1-8*(--t)*t*t*t },
  // accelerating from zero velocity
  easeInQuint: function (t) { return t*t*t*t*t },
  // decelerating to zero velocity
  easeOutQuint: function (t) { return 1+(--t)*t*t*t*t },
  // acceleration until halfway, then deceleration
  easeInOutQuint: function (t) { return t<.5 ? 16*t*t*t*t*t : 1+16*(--t)*t*t*t*t }
};


export default class Cloth extends Behavior {
  constructor(game, sprite, data) {
    super(game, sprite, data);
    this.bones = [];
  }

  get defaults() {
    return {
      bones: null,
      easing: "easeInOutQuad",
      node: 1.0,
    }
  }

  async load() {
    this.sprite.mesh.skeleton = new Skeleton();
  }

  firstUpdate() {

    let sprite = this.sprite;
    let geometry = this.sprite.mesh.geometry;

    // create the skin indices and skin weights
    let position = geometry.attributes.position;
    let vertex = new Vector3();
    let world = new Vector3();

    let skinIndices = [];
    let skinWeights = [];
    let bones = this.data.bones.map(b => new Bone());
    this.bones = bones;
    let parent = null;
    for (let x = 0; x < bones.length; x += 1) {
      let bone = bones[x];
      if (parent) {
        parent.add(bone);
        parent.updateMatrix();
      }
      parent = bone;
    }

    this.syncBones();

    let radius = this.data.node || 1.0;
    let ease = easings[this.data.easing];

    for (let i = 0; i < position.count; i ++ ) {
      vertex.fromBufferAttribute( position, i );
      vertex.add(this.sprite.position);

      // compute skinIndex and skinWeight based on some configuration data
      let weights = [];
      let total = 0;
      for (let index = 0; index < bones.length; index += 1) {
        let bone = bones[index];
        bone.getWorldPosition(world);
        // world = bone.position;
        // console.log("world", world);
        world.z = vertex.z;
        let dist = world.distanceTo(vertex);
        if (dist < radius) {
        //   // weights[index - 1].weight = 0.33;
          let weight = ease((radius - dist) / radius) * (bones.length - index + 1);
          total += weight;
          weights.push(weight);
        } else {
          weights.push(0);
        }
      }

      for (let x = 0; x < 4; x += 1) {
        if (x < 3 && total > 0) {
          skinIndices.push(x);
          skinWeights.push(weights[x] / total);
        } else {
          skinIndices.push(0);
          skinWeights.push(0);
        }
      }
    }

    geometry.setAttribute( 'skinIndex', new Uint16BufferAttribute( skinIndices, 4 ) );
    geometry.setAttribute( 'skinWeight', new Float32BufferAttribute( skinWeights, 4 ) );

    // create skinned mesh and skeleton
    let skeleton = new Skeleton( bones );
    sprite.mesh.skeleton = skeleton

    // see example from THREE.Skeleton
    let rootBone = skeleton.bones[ 0 ];
    sprite.mesh.add( rootBone );

    // bind the skeleton to the mesh
    sprite.mesh.bind( skeleton );

    // this.object = sprite.mesh;
    // this.origin = sprite.mesh.quaternion.clone();
    this.skeleton = skeleton;

    var helper = new SkeletonHelper( sprite.mesh );
    helper.material.linewidth = 3;
    this.game.scene.add( helper );
  }

  syncBones() {
    let parent = null;
    for (let x = 0; x < this.data.bones.length; x += 1) {
      let bone = this.bones[x];
      let name = this.data.bones[x];
      let sprite = this.game.getSpriteByName(name);
      if (parent) {
        bone.position.copy(parent.mesh.worldToLocal(sprite.position.clone()));
        bone.rotation.z = sprite.angle - parent.angle;
      } else {
        bone.position.copy(sprite.position);
        bone.rotation.z = sprite.angle;
      }
      parent = sprite;
    }
  }

  update() {
    if (!this._firstUpdate) {
      this._firstUpdate = true;
      this.firstUpdate();
    } else {
      this.syncBones();
    }
  }

  destroy() {
  }
}
