
import Behavior from "./base";
import {Vector3} from "three";

const TWOPI = 2 * Math.PI;

export default class Controller extends Behavior {

  get defaults() {
    return {
      speed: 5.0,
      turn_rate: 0.1,
      $decay: null,
      $effective_rate: null,
    };
  }

  constructor(game, sprite, data) {
    super(game, sprite, data);
    sprite.control = this;
  }

  get forward() {
    return new Vector3(0, 1, 0).applyAxisAngle(
        new Vector3(0, 0, 1),
        this.sprite.angle
    );
  }

  get left() {
    return new Vector3(1, 0, 0).applyAxisAngle(
        new Vector3(0, 0, 1),
        this.sprite.angle
    );
  }

  angleTo(target) {
    let pos = this.sprite.position;
    target = target.clone().sub(pos).normalize();
    let angle = Math.atan2(-target.x, target.y);
    if (Math.abs(angle) > Math.PI) {
      if (angle > 0) {
        angle = (angle - TWOPI)
      } else {
        angle = (angle + TWOPI)
      }
    }
    return angle;
  }

  relativeAngleTo(target) {
    let angle = this.angleTo(target);
    angle = (angle - this.sprite.angle) % TWOPI;
    if (Math.abs(angle) > Math.PI) {
      if (angle > 0) {
        angle = (angle - TWOPI);
      } else {
        angle = (angle + TWOPI);
      }
    }
    return angle;
  }

  turnRate() {
    return this.sprite.getCharStat("turn_rate") ** 2 * this.data.turn_rate;
  }

  turnTo(target, speed=1.0) {
    let cangle = this.sprite.body.angularVelocity || 0;
    let angle = this.relativeAngleTo(target);
    let remaining = angle; // - cangle;
    let turn_rate = this.turnRate();
    let rate = angle > 0 ? turn_rate : -turn_rate;

    if (Math.abs(remaining) <= turn_rate) {
      rate = remaining;

    // if going the correct direction
    // } else if ((cangle > 0 && angle > 0) || (cangle < 0 && angle < 0)) {
    //   let ttt = Math.abs(angle / cangle);
    //   let tts = Math.abs(cangle / (this.data.$effective_rate));

    //   if (ttt - tts <= 0) {
    //     rate = -rate;
    //   } else if (ttt - tts < 3) {
    //     rate = -rate
    //   }
    }

    // cap
    if (rate > turn_rate) {
      rate = turn_rate;
    } else if (rate < -turn_rate) {
      rate = -turn_rate;
    }

    return rate * speed;
    // return angle;
  }
}
