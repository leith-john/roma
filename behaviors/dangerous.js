import Behavior from "./base";

export default class DangerousBehavior extends Behavior {

  get defaults() {
    return {
      damage: 100,
      radius: 0.5
    };
  }

  constructor(game, sprite, data) {
    super(game, sprite, data);
  }

  update() {
    if (this.sprite.body.contacts > 0) {
      let potential = this.game.sprites.filter((s) => {
        return s.data.group != this.sprite.data.group &&
               s != this.sprite &&
               s.hp > 0
      });
      for (let sprite of potential) {
        let dist = sprite.position.distanceTo(this.sprite.position)
          - sprite.radius - this.sprite.radius;
        if (dist < this.data.radius) {
          sprite.hp -= this.data.damage;
        }
      }
    }
  }
}
