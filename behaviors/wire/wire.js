import Behavior from "../base";


const logic = {
  all() {
    return {
      name: "all",
      pipe(v) { return v.filter(a => a).length == v.length; }
    };
  },
  some() {
    return {
      name: "some",
      pipe(v) { return v.some(a => a); }
    };
  },
  count(num=3) {
    return {
      name: "count",
      pipe(v) { return v.filter(a => a).length == num; }
    };
  },
  not() {
    return {
      name: "not",
      pipe(v) { return ! v; }
    };
  },
  once_on() {
    return {
      name: "once_on",
      pipe(value, current) {
        if (this.on) {
          value = true;
        } else if (value) {
          this.on = true;
        }
        return value;
      }
    };
  },
  once() {
    return {
      name: "once",
      pipe(value, current) {
        // if it changes remember forever
        if (this.first !== undefined && this.changed !== undefined) {
          return this.changed;
        } else if (this.first === undefined) {
          this.first = value;
        } else if (value != this.first) {
          this.changed = value;
        }
        return value;
      }
    };
  },

  // out pipes
  current() {
    return {
      name: "current",
      pipe(current) { return current; },
    };
  },
  never() {
    return {
      name: "never",
      pipe() { return false; },
    };
  },
  always() {
    return {
      name: "never",
      pipe() { return false; },
    };
  },
  undefined() {
    return {
      name: "undefined",
      pipe() { return undefined; },
    };
  },
};


export default class WireBehavior extends Behavior {

  get defaults() {
    return {
      inputs: [],
      pipes: ["all"],
      pipes_out: ["current"],
      delay: 0,
      source: null,
    };
  }

  get inputs() {
    return this._inputs;
  }

  set inputs(value) {
    if (this._inputs) {
      for (let name of this._inputs) {
        let sprite = this.game.getSpriteByName(name);
        // console.log(sprite, name);
        sprite.wire.disconnect(this.sprite);
      }
    }
    this._inputs = value;
    if (! this.signals) {
      this.signals = {};
    }
    for (let name of this._inputs) {
      let sprite = this.game.getSpriteByName(name);
      // console.log(sprite, name, value);
      sprite.wire.connect(this.sprite);
      this.signals[sprite.id] = sprite.wire.current;
    }
  }

  constructor(game, sprite, data) {
    super(game, sprite, data);
    sprite.wire = this;
    this.listeners = [];
    this.handlers = [];
    this.current = undefined;
    if (! this.signals) {
      this.signals = {};
    }
    if (data.switch) {
      this.switch = this.sprite.getBehavior(data.switch);
    }
  }

  load() {
    this.pipes = this.compilePipes(this.data.pipes);
    this.pipes_out = this.compilePipes(this.data.pipes_out);
  }

  compilePipes(pipes) {
    let output = [];
    for (let pipe of pipes) {
      output.push(
        logic[pipe]()
      );
    }
    return output;
  }

  connect(listener) {
    if (listener.wire) {
      // set current
      listener.wire.signals[this.sprite.id] = this.sprite.wire.current;

      // listen for changes
      this.listeners.push(listener);
      // listener.wire.trigger(this.sprite, this.current);
    }
  }

  disconnect(listener) {
    let idx = this.listeners.indexOf(listener);
    if (idx > -1) {
      this.listeners.splice(idx, 1);
    }
    if (listener.wire) {
      delete listener.wire.signals[this.sprite.id];
    }
  }

  trigger(source, value) {
    // console.log("trigger", this.sprite.data.name, this.signals);

    this.signals[source.id] = value;

    let current = this.current;
    this.current = this.signal();

    if (current != this.current) {

      clearTimeout(this._trigger);

      this._trigger = setTimeout(() => {
        let out = this.signalOut();
        // console.log(
        //   "triggered",
        //   this.sprite.id, this.sprite.data.name,
        //   Object.values(this.signals).join(","), "=>", this.current, "=>", out,
        //   "|||", this.listeners.map(s => `${s.id}: ${s.data.name}`));
        this.signalChanged();

        if (out != undefined) {
          if (this.listeners.length > 0) {
            for (let sprite of this.listeners) {
              if (sprite.id != source.id) {
                sprite.wire.trigger(this.sprite, out);
              }
            }
          }
        }
        this.game.trigger("signal", {sprite: this.sprite, value: out});
      }, this.data.delay);
    // } else {
    //   console.log("signal sent but unchanged", this.sprite.data.name, this.sprite.data.neighbors, this.signals);
    }

    if (this.data.switch) {
      if (this.current) {
        this.switch.enabled = true;
      } else {
        this.switch.enabled = false;
      }
    }

  }

  signal() {
    let values = Object.values(this.signals);
    for (let pipe of this.pipes) {
      values = pipe.pipe(values, this.current);
    }
    return values;
  }

  signalOut() {
    let out = this.current;
    for (let pipe of this.pipes_out) {
      out = pipe.pipe(out);
    }
    return out;
  }

  signalChanged() {
  }

  update() {
    if (! this._inputs) {
      this.inputs = this.data.inputs;
    }
    if (this.data.sensor !== undefined) {
      if (this.data.sensor != this.current) {
        console.clear();
        // console.log("sensor", this.sprite.id, this.data.sensor, this.listeners);
        this.trigger(this.sprite, this.data.sensor);
      }
    }
  }
}
