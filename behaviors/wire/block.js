import Wire from "./wire.js";


const INVERSE_DIRECTIONS = {
  n: "s",
  e: "w",
  s: "n",
  w: "e",
};

export default class BlockBehavior extends Wire {
  constructor(game, sprite, data) {
    super(game, sprite, data);
    if (data.source) {
      sprite.wire.source = true;
    }
    if (data.dest) {
      sprite.wire.dest = true;
    }
  }
  trigger(source, value) {

    // i am getting a signal from this source
    // so i'm not a source for it
    // this.disconnect(source);

    return super.trigger(source, value);
  }
  signalChanged() {
    this.data.updated = this.game.time;
    if (this.current) {
      this.data.opacity = 1;
    } else {
      this.data.opacity = 0.0;
    }
    // console.log("signal", this.current);

    if (this.data.events) {
      let state = this.current ? "on" : "off";
      for (let event of this.data.events.filter(e => e.event == state)) {
        // this.sprite.timeline.cleanEvents(event.trait);
        // console.log(event.trait, event.delay, event.value);
        this.sprite.timeline.add(event.trait, event.delay, event.value);
      }
    }
  }
  onMapping(data) {
    this.sprite.data.coords = {x: data.x, y: data.y, z: data.z};
    this.sprite.data.neighbors = data.neighbors;
    this.sprite.data.neighborCount = Object.values(data.neighbors).filter(a => a).length;
  }
  onMapped() {
    // if (!this.sprite.wire.dest) {
      // this.path(this.sprite, "");
      for (let direction in this.sprite.data.neighbors) {
        // ignore source direction
        let n = this.sprite.data.neighbors[direction];
        if (n && n.wire && n.id != this.sprite.id) {

          // wire
          n.wire.connect(this.sprite);
          // console.log(this.sprite.data.coords, direction, n.id);
        }
        if (n && n.id == this.sprite.id) {
          console.log("attempted to self attach", this.sprite, this.sprite.data.textmapCoords, direction);
        }
      }
    // }
    this.signals = {};
  }
  // update() {
  //   let current = this.sprite.mesh.material.opacity
  //   if (this.data.opacity != current) {
  //     let p = (this.game.time - this.data.updated) / (this.data.delay * 1.1);
  //     if (p >= 1) {
  //       this.sprite.mesh.material.opacity = this.data.opacity;
  //     } else {
  //       this.sprite.mesh.material.opacity = current + (this.data.opacity - current) * p;
  //     }
  //   }
  //   return true;
  // }
  // path(sprite, from) {
  //   sprite.data.blockPathed = true;

  //   if (sprite.data.neighbors) {
  //     for (let direction in sprite.data.neighbors) {
  //       // ignore source direction
  //       if (direction != from) {
  //         let n = sprite.data.neighbors[direction];
  //         if (n && n.wire) {

  //           // wire
  //           n.wire.connect(sprite);

  //           // console.log("wire", direction);
  //           if (! n.wire.source) {
  //             this.path(n, INVERSE_DIRECTIONS[direction]);
  //           }
  //         }
  //       }
  //     }
  //   }
  // }
}

