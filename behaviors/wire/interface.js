import Wire from "./block.js";


export default class InterfaceBehavior extends Wire {

  constructor(game, sprite, data) {
    super(game, sprite, data);
    console.log("interface created")
    this.testedNearby = false;
  }

  trigger(source, value) {
    super.trigger(source, value);
    console.log("trigger", source, value, this.current, this.signals);
  }

  signalChanged() {
    // if (this.current) {
      // find neighbors
      console.log("interface signaled", this.signals, this.current);
      if (!this.testedNearby) {
        console.log("is anyone near?")
        this.testedNearby = true;

        // TODO: there needs to be a faster way to do this
        // maybe only do interfaces?
        for (let sprite of this.game.sprites) {
          if (sprite.id != this.sprite.id &&
              sprite.wire &&
              sprite.wire instanceof InterfaceBehavior &&
              // ! this.signals[sprite.id] &&
              // this.listeners.indexOf(sprite) == -1 &&
              sprite.position.distanceTo(this.sprite.position) < 1.1) {

              // this.listeners.indexOf(sprite) == -1 &&
            console.log("this is close", sprite);
            this.connect(sprite);
          }
        }
      // }
    }

    this.data.updated = this.game.time;
    if (this.current) {
      this.data.opacity = 1;
    } else {
      this.data.opacity = 0.0;
    }

    let mesh = this.sprite.mesh;
    if (mesh) {
      this.sprite.timeline.cleanEvents("opacity");
      if (this.current) {
        this.sprite.timeline.add("opacity", 100, 1);
      } else {
        this.sprite.timeline.add("opacity", 100, 0);
      }
    }
  }
}

