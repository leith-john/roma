import WireBehavior from "./wire";


export default class SwitchBehavior extends WireBehavior {

  constructor(game, sprite, data) {
    super(game, sprite, data);
    this.target = this.sprite.getBehavior(data.target);
  }

  trigger(source, value) {
    super.trigger(source, value);

    if (this.current) {
      this.target.enabled = true;
    } else {
      this.target.enabled = false;
    }
  }
}
