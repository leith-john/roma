import Behavior from "../base";


export default class SensorBehavior extends Behavior {

  constructor(game, sprite, data) {
    super(game, sprite, data);
    this.touching = false;
  }

  update() {
    if (this.sprite.body.contacts === 0 && this.touching) {
      console.log("on", this.sprite.body.contacts, this.touching);
      this.touching = false;
      this.sprite.wire.trigger(this, false);
    }
    if (this.sprite.body.contacts && ! this.touching) {
      console.log("off", this.sprite.body.contacts, this.touching);
      this.touching = true;
      this.sprite.wire.trigger(this, true);
    }
  }
}
