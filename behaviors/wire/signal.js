import Behavior from "../base";

export default class SignalingBehavior extends Behavior {

  constructor(game, sprite, data) {
    super(game, sprite, data);
    this.handlers = [];
  }

  signalChanged(value) {
    for (let handler of this.handlers) {
      handler(this.sprite, value);
    }
    this.game.trigger("signal", {sprite: this.sprite, value: value});
  }

  onSignalChanged(handler) {
    this.handlers.push(handler);
  }
}
