import Behavior from "./wire";


export default class SensorBehavior extends Behavior {

  constructor(game, sprite, data) {
    super(game, sprite, data);
    this.light = this.sprite.getBehavior("three/light").light;
    this.intensity = this.light.intensity;
  }

  signalChanged(value) {
    console.log("signal changed", value);
    if (value) {
      this.light.intensity = this.intensity;
    } else {
      this.light.intensity = 0.1;
    }
  }
}
