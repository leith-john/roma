import Behavior from "./base";
import Controller from "./controller";
import GotoController from "./goto_cursor";


/**
 * The controller creates a cursor and this object tries to go there
 */
export default class GotoOtherController extends GotoController {
  update() {

    for (let sprite of this.game.sprites.filter(this.test.bind(this))) {
      this.sprite.states.cursor = sprite.position.clone();
      this.sprite.states.go = true;
      this.sprite.states.fire = true;
      break;
    }

    super.update();
  }

  test(s) {
    return s.data.group &&
      s.data.group != this.sprite.data.group &&
      s.hp;
  }
}
