import {
  BoxGeometry,
  BufferAttribute,
  BufferGeometry,
  CircleGeometry,
  Euler,
  FrontSide,
  Line,
  LineDashedMaterial,
  Mesh,
  MeshBasicMaterial,
  MeshPhysicalMaterial,
  MeshStandardMaterial,
  MeshToonMaterial,
  Object3D,
  PlaneGeometry,
  Quaternion,
  ShaderMaterial,
  Shape,
  ShapeGeometry,
  SkinnedMesh,
  SphereGeometry,
  Sprite,
  Vector2,
  Vector3,
} from "three";
import Behavior from "../base";

// for cutout
import copy from "@virgodev/bazaar/functions/copy";
import Delaunay from "delaunay-fast";
import outline from "image-outline";
import { MeshBehaviorDef } from "../../engine/defs";
import Game from "../../engine/game";

export const geometries = {
  empty(data) {
    return new BufferGeometry();
  },
  polygon(data) {
    const points = [];
    for (const v of data.vertices) {
      points.push(new Vector3(v.x, v.y, v.z));
    }
    return new BufferGeometry().setFromPoints(points);
  },
  shape(data) {
    const shape = new Shape(
      data.vertices
        ? data.vertices.map((p) => new Vector2(p.x, p.y))
        : undefined,
    );
    if (data.path) {
      for (const part of copy(data.path)) {
        const type = part.shift();
        if (type === "move") {
          shape.moveTo(...part);
        } else if (type === "line") {
          shape.lineTo(...part);
        } else if (type === "bezier") {
          shape.bezierCurveTo(...part);
        } else if (type === "quad") {
          shape.quadraticCurveTo(...part);
        }
      }
    }
    return new ShapeGeometry(shape);
  },
  circle(data) {
    return new CircleGeometry(
      data.radius || 0.5,
      data.points || 8,
      Math.PI / 2,
    );
  },
  sphere(data) {
    return new SphereGeometry(
      data.radius || 0.5,
      data.points || 8,
      data.points || 8,
    );
  },
  square(data) {
    return new PlaneGeometry(
      data.width,
      data.height,
      data.widthSegments || data.segments || 1,
      data.heightSegments || data.segments || 1,
    );
  },
  box(data) {
    const geometry = new PlaneGeometry(
      data.width,
      data.height,
      data.widthSegments || data.segments || 1,
      data.heightSegments || data.segments || 1,
    );
    if (data.scale) {
      geometry.scale(data.scale.x, data.scale.y, data.scale.z);
    }
    return geometry;
  },
  cube(data) {
    return new BoxGeometry(
      data.width,
      data.height,
      data.depth || 1,
      data.widthSegments || data.segments || 1,
      data.heightSegments || data.segments || 1,
    );
  },
  model(data) {
    return this.models[data.model];
  },
  // text(data) {
  //   let size = data.size || 1;
  //   let text = data.text || "Hello World";
  //   let font = data.font || "default";

  //   console.log("font", data, this.fonts);

  //   // create a div for measuring
  //   // create a canvas for rendering
  //   // render to an image
  //   // use as a map

  //   let geometry = new TextGeometry(text, {
  //     name: data.name,
  //     // font: this.fonts[font],
  //     size: size,
  //     height: size / 10,
  //     curveSegments: 12,

  //     // makes the text fat
  //     // bevelEnabled: true,
  //     // bevelThickness: 1,  // size / 10,
  //     // bevelSize: size / 100,
  //     // bevelOffset: 0,
  //     // bevelSegments: 1
  //   });
  //   return geometry;
  // },
  cutout(data, modelData) {
    const texture = this.textures[modelData.material.map];
    const w = texture.image.width;
    const h = texture.image.height;

    // TODO: remove this
    // modelData.material.map = null;

    let shape = outline(texture.image);

    // add inner vertices
    if (data.segments > 1) {
      // segment outside
      const count = Math.floor(data.segments);
      const scale = 1;
      const minDist = 30;
      const shapeNext = [];
      for (let s = 1; s < shape.length; s += 1) {
        const point1 = new Vector2(shape[s].x, shape[s].y);
        const point2 = new Vector2(shape[s - 1].x, shape[s - 1].y);
        const dist = point1.distanceTo(point2);
        if (dist > minDist * 2) {
          const segments = Math.floor(
            Math.min(dist / minDist, dist / (count - 1)),
          );
          for (let d = 1; d <= segments; d += 1) {
            const lerped = point2.clone().lerp(point1, d / (segments + 1));
            shapeNext.push({
              x: lerped.x,
              y: lerped.y,
            });
          }
        }
      }
      shape = shape.concat(shapeNext);

      // scale inside
      const inner = [];
      for (let s = 1; s < count; s += 1) {
        for (const point of shape) {
          inner.push(
            new Vector2(
              (point.x - w / 2) * ((count - s) / count) * scale + w / 2,
              (point.y - h / 2) * ((count - s) / count) * scale + h / 2,
            ),
          );
        }
      }

      // remove duplicates
      while (inner.length > 0) {
        const point = inner.pop();
        let dups = [1];
        let count = 1;
        while (dups.length > 0) {
          dups = inner.filter((i) => i.distanceTo(point) <= minDist);
          for (const dup of dups) {
            point.x += dup.x;
            point.y += dup.y;
            count += 1;
            inner.splice(inner.indexOf(dup), 1);
          }
        }
        point.x /= count;
        point.y /= count;
        shape.push({ x: point.x, y: point.y });
      }
    }

    const triangles = Delaunay.triangulate(shape.map((p) => [p.x, p.y]));
    const geometry = new BufferGeometry();
    const uv = [];
    const points = [];
    for (const p of shape) {
      points.push(
        (p.x / w) * data.width - data.width / 2,
        (-p.y / h) * data.height + data.height / 2,
        0,
      );
      uv.push(p.x / w, 1 - p.y / h);
    }
    geometry.setAttribute(
      "position",
      new BufferAttribute(new Float32Array(points), 3),
    );
    geometry.setAttribute("uv", new BufferAttribute(new Float32Array(uv), 2));
    geometry.setIndex(triangles);
    return geometry;
  },
};

export var materials = {
  base(data) {
    let map = this.textures[data.map] || null;
    let material = {
      // skinning: data.skinning || false,
      color: data.color || "white",
      map: map,
      side: data.side || FrontSide,
      flatShading: data.flat ? true : false,
      opacity: data.opacity === undefined ? 1 : data.opacity,
      transparent:
        map || data.transparent || data.opacity !== undefined ? true : false,
      depthTest: data.depthTest !== undefined ? data.depthTest : true,
      visible: data.visible !== undefined ? data.visible : true,
      vertexColors: !!data.vertexColors,
      wireframe: !!data.wireframe,
    };
    if (data.emissive) {
      material.emissive = data.emissive;
    }
    return material;
  },
  imported(data) {
    return this.materials[data.name];
  },
  basic(data) {
    let material = materials.base.bind(this)(data);
    delete material.flatShading;
    // delete material.skinning;
    return new MeshBasicMaterial(material);
  },
  standard(data) {
    let material = materials.base.bind(this)(data);
    // console.log("standard material", material);
    return new MeshStandardMaterial(material);
  },
  physical(data) {
    let material = materials.base.bind(this)(data);
    material.metalness = data.metalness || 0.5;
    material.roughness = data.roughtness || 0.5;
    // console.log("material", material);
    return new MeshPhysicalMaterial(material);
  },
  toon(data) {
    let material = materials.base.bind(this)(data);
    material.metalness = data.metalness || 0.5;
    material.roughness = data.roughtness || 0.5;
    // console.log("material", material);
    return new MeshToonMaterial(material);
  },
  line(data) {
    let material = materials.base.bind(this)(data);
    material.linewidth = data.linewidth || 1;
    material.linecap = data.linecap || "round";
    material.linejoin = data.linejoin || "round";
    material.dashSize = data.dashSize || 10;
    material.gapSize = data.gapSize || 5;
    material.scale = data.scale || 0.001;
    delete material.skinning;
    delete material.map;
    return new LineDashedMaterial(material);
  },
  custom(data) {
    return new ShaderMaterial({
      vertexShader: data.vertex,
      fragmentShader: data.fragment,
    });
  },
};

let meshCache = {};
let geometryCache = {};

export default class MeshBehavior extends Behavior {
  get defaults(): Partial<MeshBehaviorDef> {
    return {
      static: false,
      cached: true,
      material: { type: "basic", color: 0x00ff00, cached: true },
    };
  }

  constructor(game: Game, sprite: Sprite, data: MeshBehaviorDef) {
    super(game, sprite, data);

    if (typeof data.shape == "object") {
      // standalone reference
      this.shape = data.shape;
    } else {
      // reference from shapes
      this.shape = sprite.data.shapes[data.shape];
    }
    this.sprite.mesh = this.createMeshFromShape(this.shape);
    if (!data.static) {
      this.sprite.renderers.push(this);
    }
  }

  createMeshFromShape(shape) {
    let material = shape.material || this.data.material;
    let geometry = null;

    // build geometry
    let key = this.sprite.data.name;
    if (shape.name) {
      geometry = this.game.geometry[shape.name];
    } else {
      if (!geometries[shape.type]) {
        console.error("Missing shape", shape.type);
      }
      geometry = geometries[shape.type].bind(this.game)(shape, this.data);
    }
    if (shape.center) {
      geometry._center = shape.center;
      geometry.translate(
        -shape.center.x,
        -shape.center.y,
        -shape.center.z || 0,
      );
    }

    // build material
    if (material) {
      if (typeof material == "string") {
        material = this.game.materials[material];
      } else if (material.name) {
        // look in material cache
        if (this.game.materials[material.name]) {
          material = this.game.materials[material.name];
        } else {
          material = materials[material.type || "basic"].bind(this.game)(
            material,
          );

          // save in cache
          this.game.materials[material.name] = material;
        }
      } else {
        material = materials[material.type || "basic"].bind(this.game)(
          material,
        );
      }
    }

    let testMaterial = material;
    if (Array.isArray(testMaterial)) {
      testMaterial = testMaterial[0];
    }

    let mesh = null;
    if (shape.type === "empty") {
      mesh = new Object3D();
    } else if (shape.skeleton) {
      mesh = new SkinnedMesh(geometry, material);
      // let skeleton = this.game.skeletons[shape.skeleton];
      // mesh.add(skeleton.bones[0]);
      // mesh.bind(skeleton);
      // mesh.bindMode = "attached";
    } else if (shape.line) {
      mesh = new Line(geometry, material);
    } else {
      mesh = new Mesh(geometry, material);
    }
    mesh.userData = this.sprite;

    if (
      (this.data.shadows === undefined || this.data.shadows) &&
      !this.data.material.emissive
    ) {
      mesh.castShadow = true;
      mesh.receiveShadow = true;
    }
    mesh.position.copy(this.sprite.position);
    mesh.rotation.z = this.sprite.angle;
    if (this.data.visible !== undefined) {
      mesh.visible = this.data.visible;
    }

    let scale = this.data.scale || 1;
    if (typeof scale === "object") {
      mesh.scale.set(scale.x, scale.y, scale.z);
    } else {
      mesh.scale.set(scale, scale, scale);
    }
    if (key && this.data.static) {
      if (!meshCache[key]) {
        let empty = new Mesh(new BufferGeometry(), material);
        empty.castShadow = true;
        empty.receiveShadow = true;
        meshCache[key] = empty;
        this.game.scene.add(empty);
      }

      meshCache[key].geometry.mergeMesh(mesh);
      this.game.scene.add(meshCache[key]);
      mesh = null;

      clearTimeout(meshCache[key]._timer);
      meshCache[key]._timer = setTimeout(() => {
        let mesh = meshCache[key];
        this.game.scene.remove(mesh);
        mesh = new Mesh(
          new BufferGeometry().fromGeometry(mesh.geometry),
          mesh.material,
        );
        mesh.castShadow = true;
        mesh.receiveShadow = true;
        this.game.scene.add(mesh);
      }, 1000);
    } else {
      let parent: Object3D = this.game.renderers[0].scene;
      if (this.data.parent) {
        parent = this.game.getSpriteByName(this.data.parent).mesh;
      }
      parent.add(mesh);
      mesh.sprite = this.sprite;
    }

    if (shape.children) {
      for (let child of shape.children) {
        let childShape = this.sprite.data.shapes[child];
        child = this.createMeshFromShape(childShape);
        if (childShape.position) {
          child.position.copy(childShape.position);
        } else {
          child.position.set(0, 1, 0);
        }
        if (childShape.angle) {
          child.rotation.z = shape.angle;
        } else {
          child.rotation.z = 0;
        }
        // mesh.add(child);
      }
    }

    return mesh;
  }

  fixAngle(a) {
    a = a % (Math.PI * 2);
    if (a < 0) {
      a = Math.PI * 2 + a;
    }
    return a;
  }

  update() {}

  draw() {
    if (!this.data.static && this.sprite.mesh) {
      let pos = this.sprite.position.clone();
      let angle = this.sprite.angle;
      let orientation = this.sprite.orientation;

      if (!this.target) {
        let next = this.sprite.timeline.next("position");
        if (next) {
          this.target = next;
        }
      }
      if (this.target) {
        let p = this.sprite.timeline.pval(this.target);
        if (p > 0) {
          pos.lerp(this.target.value.position, p);

          let b = new Quaternion();
          b.setFromAxisAngle(new Vector3(0, 0, 1), this.target.value.angle);
          let a = this.sprite.mesh.quaternion.clone().slerp(b, p);
          angle = new Euler().setFromQuaternion(a).z;

          if (p >= 1) {
            this.sprite.position.copy(this.target.value.position);
            this.sprite.angle = angle;
            this.target = null;
          }
        }
      }

      this.sprite.mesh.position.copy(pos);
      if (orientation) {
        this.sprite.mesh.quaternion.copy(orientation);
      } else {
        this.sprite.mesh.rotation.z = angle;
      }

      let nextOpacity = this.sprite.timeline.next("opacity");
      if (nextOpacity) {
        if (nextOpacity.start === undefined) {
          nextOpacity.start = this.sprite.mesh.material.opacity;
        }
        this.sprite.mesh.material.opacity = this.sprite.timeline.lerp(
          nextOpacity.start,
          nextOpacity,
        );
      }

      let nextScale = this.sprite.timeline.next("scale");
      if (nextScale) {
        if (typeof nextScale !== "object") {
          if (!nextScale.start) {
            nextScale.start = this.sprite.mesh.scale.x;
          }
          this.sprite.mesh.scale.x = this.sprite.timeline.lerp(
            nextScale.start,
            nextScale,
          );
          this.sprite.mesh.scale.y = this.sprite.mesh.scale.x;
          this.sprite.mesh.scale.z = this.sprite.mesh.scale.x;
        } else {
          if (!nextScale.start) {
            nextScale.start = {
              x: this.sprite.mesh.scale.x,
              y: this.sprite.mesh.scale.y,
              z: this.sprite.mesh.scale.z,
            };
          }
          this.sprite.mesh.scale.x = this.sprite.timeline.lerp(
            nextScale.start.x,
            {
              value: nextScale.value.x,
              end: nextScale.end,
              duration: nextScale.duration,
            },
          );
          this.sprite.mesh.scale.y = this.sprite.timeline.lerp(
            nextScale.start.y,
            {
              value: nextScale.value.y,
              end: nextScale.end,
              duration: nextScale.duration,
            },
          );
          this.sprite.mesh.scale.z = this.sprite.timeline.lerp(
            nextScale.start.z,
            {
              value: nextScale.value.z,
              end: nextScale.end,
              duration: nextScale.duration,
            },
          );
        }
      }
    }
  }

  destroy() {
    if (!this.data.static) {
      this.game.scene.remove(this.sprite.mesh);
      // TODO: this.sprite.mesh.geometry.dispose();
      // TODO: this.sprite.mesh.material.dispose();
      this.sprite.mesh = null;
    }
  }
}
