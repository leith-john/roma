import { Color } from "three";
import { GridHelper } from "../../utils/grid";
import Behavior, { BehaviorData } from "../base";

export interface GridMeshData extends BehaviorData {
  name: "three/grid";
  width: number;
  height: number;
  divisions?: [number, number];
  centerColor?: string | number | Color;
  color?: string | number | Color;
  rotation?: { x: 0; y: 0; z: 0 };
  opacity?: number;
}

export default class GridMeshBehavior extends Behavior<GridMeshData> {
  helper?: GridHelper;

  get defaults(): Partial<GridMeshData> {
    return {
      divisions: [10, 10],
      centerColor: "white",
      color: "grey",
    };
  }

  async load(): Promise<void> {
    this.helper = new GridHelper(
      this.data.width,
      this.data.height,
      this.data.divisions[0],
      this.data.divisions[1],
      this.data.centerColor || this.data.color,
      this.data.color,
    );
    if (this.data.opacity) {
      this.helper.material.transparent = true;
      this.helper.material.opacity = this.data.opacity;
    }
    this.helper.userData = this.sprite;
    if (this.data.rotation) {
      this.helper.rotation.x = this.data.rotation.x;
      this.helper.rotation.y = this.data.rotation.y;
      this.helper.rotation.z = this.data.rotation.z;
    }
    if (this.sprite.mesh) {
      this.sprite.mesh.add(this.helper);
    } else {
      this.sprite.mesh = this.helper;
      this.helper.position.copy(this.sprite.position);
      this.game.renderer.scene.add(this.helper);
    }
  }

  update() {}

  draw() {}

  destroy() {
    if (this.helper) {
      this.game.renderer.scene.remove(this.helper);
      this.helper.dispose();
    }
  }
}
