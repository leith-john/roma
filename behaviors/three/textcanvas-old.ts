import Behavior, { BehaviorData } from "../base";

export interface TextCanvasData extends BehaviorData {
  size?: string;
  text?: string;
  font?: string;
  texture: string;
}

export default class TextCanvas extends Behavior<TextCanvasData> {
  get defaults(): Partial<TextCanvasData> {
    return {
      size: "30px",
      text: "hello",
      font: "verdana",
    };
  }

  async load() {
    // create a div for measuring
    const div = document.createElement("div");
    document.body.appendChild(div);
    div.style.visibility = "hidden";
    div.style.width = "max-content";
    div.style.zIndex = 1;
    div.style.fontSize = this.data.size;
    div.style.padding = "0px";
    div.style.fontFamily = this.data.font;
    div.style.whiteSpace = "pre";
    div.style.display = "block";
    div.innerHTML = this.data.size;

    getComputedStyle(div);
    const box = div.getBoundingClientRect();
    const width = box.width;
    const height = box.height;
    let map = null;
    // console.log(`text: [${text}]`, box.width);

    // create a canvas for rendering
    let textureName = null;
    if (this.data.text.trim()) {
      textureName = `glyph-${this.data.font}-${this.data.size}-${this.data.text}`;
      if (!this.game.glyphs) {
        this.game.glyphs = {};
      }
      if (!this.game.glyphs[textureName]) {
        const canvas = document.createElement("canvas");
        canvas.style.background = "transparent";
        canvas.width = width;
        canvas.height = height;
        this.data.width = width;

        // draw text on the box
        const ctx = canvas.getContext("2d");
        if (ctx) {
          ctx.font = `${this.data.size} ${this.data.font}`;
          // console.log("size", size, font);

          if (this.data.shadow) {
            ctx.shadowColor = this.data.shadow;
            ctx.shadowBlur = parseInt(this.data.shadowLength.slice(0, -2)) || 5;
          }
          ctx.lineWidth = 1;
          ctx.strokeStyle = this.data.foreground || "white";
          ctx.fillStyle = this.data.foreground || "white";
          const ts = ctx.measureText(this.data.text);
          ctx.fillText(this.data.text, 0, ts.fontBoundingBoxAscent);
          // ctx.strokeText(text, 0, ts.fontBoundingBoxAscent);
          // console.log("ts", ts, this.data);
        }

        // render to an image
        map = canvas.toDataURL();
        console.log("map", map);
        // console.log("map", text, map);

        if (map) {
          this.game.glyphs[textureName] = await this.game.load([
            {
              type: "texture",
              name: textureName,
              src: map,
            },
          ]);
        }
      }
    }
    // document.body.removeChild(div);

    if (textureName) {
      const behavior = this.sprite.addBehavior({
        name: "three/mesh",
        shape: {
          type: "box",
          width: width,
          height: height,
          // center: { x: -width / 2, y: -height / 2 },
          scale: { x: 1, y: -1, z: 1 },
        },
        material: {
          map: textureName,
          color: "white",
          // side: DoubleSide,
          transparent: true,
          opacity: 0.9,
        },
        visible: this.data.visible,
      });
      if (behavior.postLoad) {
        behavior.postLoad();
      }
    } else {
      this.sprite.mesh = {};
    }
    this.sprite.mesh.bounds = { x: box.width, y: box.height };
  }

  draw() {
    console.log("draw");
  }

  update() {
    // console.log("update", this.sprite.mesh.scale.x);
    // this.sprite.mesh.position.x = this.sprite.position.x;
    // this.sprite.mesh.position.y = this.sprite.position.y;
    return false;
  }

  destroy() {
    if (this.body) {
      for (const child of this.body) {
        console.log("child", child);
        if (child.destroy) {
          child.destroy();
        } else if (child.dispose) {
          child.dispose();
        }
      }
    }
  }
}

TextCanvas.failIndices = {};

console.log("register");
TextCanvas.register("three/textcanvas");
