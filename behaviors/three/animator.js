import {AnimationMixer, AnimationClip, ObjectLoader} from "three";

import Behavior from "../base";
import merge from "../../utils/merge";

export default class Animator extends Behavior {

  get defaults() {
    return {
      tracks: {

      }
    };
  }

  constructor(game, sprite, data) {
    super(game, sprite, data);
  }

  async load() {
    let anim = new AnimationMixer(this.game.scene);
    let clips = this.sprite.mesh.geometry.animations;
    console.log(clips);
    // let action = anim.clipAction(this.game.clip);
    // action.play();

    // Play all animations
    if (clips) {
      clips.forEach( function ( clip ) {
        console.log("clip", clip);
        if (clip.name == "Wave") {
          anim.clipAction( clip ).play();
        }
      });
    }

    this.anim = anim;
  }

  update() {
    this.anim.update(30/1000);
    return true;
  }

  destroy() {
  }
}
