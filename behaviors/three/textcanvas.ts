import { nanoid } from "nanoid";
import { LinearFilter, Object3D } from "three";
import { textSplitter } from "../../../cyoa-next/src/utils/textSplitter";
import { MeshBehaviorDef, TextureData } from "../../engine/defs";
import Sprite, { SpriteData } from "../../engine/sprite";
import { CharData, CharRenderer } from "../../utils/chars";
import Behavior, { BehaviorData } from "../base";

export interface TextCanvasData extends BehaviorData {
  name: "three/textcanvas";
  size?: number;
  text?: string;
  font?: string;
  width: number;
  height: number;
  foreground?: string;
  padding?: number;
  fill?: boolean;
  justify?: "center" | "left" | "right";
  align?: "center" | "top" | "bottom";
}

export default class TextCanvas extends Behavior<TextCanvasData> {
  textRoot: Object3D = new Object3D();
  renderer: CharRenderer;
  chars: Sprite[] = [];
  currentWidth: number = 0;
  currentHeight: number = 0;
  textWidth: number = 0;
  textHeight: number = 0;

  static glyphs: Record<string, CharData> = {};

  get defaults(): Partial<TextCanvasData> {
    return {
      size: 1,
      text: "(empty)",
      font: "verdana",
      justify: "center",
      align: "center",
      padding: 0.5,
      fill: true,
      foreground: "white",
    };
  }

  async load() {
    this.renderer = new CharRenderer();

    // console.log(`text: [${text}]`, box.width);
    await this.renderText();

    this.textRoot.userData = this.sprite;
    if (this.sprite.mesh) {
      this.sprite.mesh.add(this.textRoot);
    } else {
      this.textRoot.position.copy(this.sprite.position);
      this.game.renderer.scene.add(this.textRoot);
    }

    // this.sprite.renderers.push(this);
  }

  // draw() {}

  update() {
    // return false;
  }

  async renderChar(char: string): Promise<string> {
    const data = await this.renderer.renderCharacter(char, {
      size: 240,
      font: this.data.font || "verdana",
    });
    await this.game.load([
      {
        type: "texture",
        name: `glyph-${char}`,
        src: data.data,
        minFilter: LinearFilter,
        magfilter: LinearFilter,
        anistropy: 16,
        generateMipMaps: true,
      } as TextureData,
    ]);
    TextCanvas.glyphs[char] = {
      ...data,
      data: `glyph-${char}`,
    };
    return `glyph-${char}`;
  }

  async renderMesh(char: string): Promise<Sprite> {
    const data = TextCanvas.glyphs[char];
    const glyphScale = this.data.size / 240;
    const glyphWidth = data.width * glyphScale;
    const glyphHeight = data.height * glyphScale;
    // const geom = new PlaneGeometry(glyphWidth, glyphHeight);
    // geom.translate(glyphWidth * 0.5, -glyphHeight * 0.5, 0);

    // TODO: turn these into sprites
    // TODO: animate them

    const [sprite]: Sprite[] = await this.game.load([
      {
        type: "sprite",
        name: "glyph-" + nanoid(),
        behaviors: [
          {
            name: "three/mesh",
            shape: {
              width: glyphWidth,
              height: glyphHeight,
              type: "square",
              // center: { x: glyphWidth * 0.5, y: glyphHeight * 0.5, z: 0 },
            },
            material: {
              map: data.data,
              color: this.data.foreground,
              transparent: true,
              type: "basic",
            },
          } as MeshBehaviorDef,
          // {
          //   name: "layout/boundary",
          //   width: glyphWidth,
          //   height: glyphHeight,
          // },
        ],
        width: glyphWidth,
        height: glyphHeight,
      } as SpriteData,
    ]);

    // const mesh = new Mesh(
    //   geom,
    //   new MeshBasicMaterial({
    //     map: this.game.textures[data.data],
    //     color: this.data.foreground,
    //     transparent: true,
    //     // depthTest: false,
    //   }),
    // );
    // const blurMaterial = createBlurMaterial(
    //   this.game.textures[data.data],
    //   new Color(0, 0, 0),
    //   3,
    // );
    // const shadow = new Mesh(geom, blurMaterial);
    // shadow.position.x = 0.005;
    // shadow.position.y = -0.005;
    // shadow.position.z = -0.01;
    // mesh.add(shadow);
    if (sprite.mesh) {
      sprite.mesh.userData = {
        ...data,
        width: glyphWidth,
        height: glyphHeight,
      };
    }
    return sprite;
  }

  async renderText() {
    // if (this.data.text) {
    this.clearText();

    let x = 0;
    let p = this.data.padding || 0;
    let maxx = this.data.width - p * 2;
    let maxy = this.data.height - p * 2;
    let w = 0;
    let lh = 0;
    const lines: { w: number; h: number; sprites: Sprite[] }[] = [];
    let line: Sprite[] = [];
    let sprites: Sprite[] = [];
    const words = textSplitter(this.data.text || "....");

    let chars = "";
    let idx = 0;
    for (const word of words) {
      w = 0;
      sprites = [];
      for (const char of word.content) {
        chars += char;
        // render text if not done
        if (char === "\n") {
          // new line
        } else if (!TextCanvas.glyphs[char]) {
          await this.renderChar(char);
        }

        // generate mesh
        if (TextCanvas.glyphs[char]) {
          const sprite = await this.renderMesh(char);
          sprite.timeline.add("opacity", 0, 0);
          sprite.timeline.add(
            "opacity",
            500 + Math.random() * 500,
            1,
            idx * 30,
          );
          idx += 1;
          w += sprite.data.width;
          lh = Math.max(lh, sprite.data.height);
          sprites.push(sprite);
        }
      }
      if (/\n/.test(chars) || (this.data.width && x + w > maxx)) {
        lines.push({ w: x, h: lh, sprites: line });
        line = [];
        chars = "";
        x = 0;
      }

      if (x !== 0 || word.type !== "whitespace") {
        x += w;
        line = [...line, ...sprites];
      }
    }
    if (line.length > 0) {
      lines.push({ w: x, h: lh, sprites: line });
    }

    const th = lines.reduce((a, b) => a + b.h, 0);
    const tw = lines.reduce((a, b) => Math.max(a, b.w), 0);

    let y = 0;
    if (this.data.align === "top") {
      y = maxy / 2;
    } else if (this.data.align === "center") {
      y = th / 2;
    } else if (this.data.align === "bottom") {
      y = -maxy / 2 + th;
    }

    for (const line of lines) {
      let cursor = 0;

      if (this.data.justify === "right") {
        cursor = maxx / 2 - line.w;
      } else if (this.data.justify === "center") {
        cursor = -line.w / 2;
      } else {
        cursor = -maxx / 2;
      }
      // console.log(" -", chars.trim(), x, "+", w, "/", y, this.data);
      let first = true;
      for (const sprite of line.sprites) {
        sprite.position.x += cursor + sprite.data.width / 2;
        sprite.position.y = y - sprite.data.height / 2;
        sprite.position.z = 0.02;

        cursor += sprite.data.width;
        if (sprite.mesh) {
          this.textRoot.add(sprite.mesh);
          this.chars.push(sprite);
          sprite.mesh.userData = this.sprite;
        }
      }
      y -= line.h;
    }
    // if (this.data.fill) {
    //   this.renderBackground(this.data.width, this.data.height);
    // } else {
    //   this.renderBackground(tw + p * 2, th + p * 2);
    // }

    this.textWidth = tw;
    this.textHeight = th;
  }

  async clearText() {
    /** remove all created meshes from scene and dispose of them */
    for (const sprite of this.chars) {
      if (sprite.mesh) {
        this.textRoot.remove(sprite.mesh);
        await sprite.destroy();
      }
    }
    this.chars = [];
  }

  destroy() {
    this.clearText();
    for (const sprite of this.chars) {
      if (sprite.mesh) {
        this.textRoot.remove(sprite.mesh);
        sprite.destroy();
      }
    }
    this.chars.length = 0;
    if (this.renderer) {
      this.renderer.dispose();
    }
    // if (this.sprite.mesh) {
    //   this.sprite.mesh.remove(this.textRoot);
    // } else {
    this.game.renderer.scene.remove(this.textRoot);
    // }
  }
}

TextCanvas.register("three/textcanvas");

if (import.meta.hot) {
  import.meta.hot.dispose((mod) => {
    TextCanvas.hotDispose<TextCanvas>("three/textcanvas");
  });

  import.meta.hot.accept((mod) => {
    if (mod) {
      TextCanvas.glyphs = {};
      TextCanvas.hotAccept<TextCanvas>("three/textcanvas", mod.default);
    }
  });
}
