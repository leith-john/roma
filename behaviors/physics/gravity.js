import Behavior from "../base";


export default class Gravity extends Behavior {
  update() {
    this.sprite.body.impulse({x: this.data.x, y: this.data.y});
  }
}
