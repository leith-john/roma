/* globals window THREE */

import Behavior from "../base";

const TWOPI = Math.PI * 2;


class JointProxy {
  constructor(game, behavior) {
    this.behavior = behavior;
    this.game = game;
  }

  get id() {
    return this.behavior.data.id;
  }

  update(data) {
    data.id = this.id;
    this.game.world.changeJoint(data);
  }
}

export default class Joint extends Behavior {
  constructor(game, sprite, data) {
    super(game, sprite, data);
    sprite.joint = new JointProxy(game, this);
  }

  async load() {
    await this.game.world.createJoint(this.sprite, this.data);
    this.origin = this.sprite.position.clone();
    this.angle = this.sprite.angle;
  }

  update() {

    if (this.data.spring) {
      // return to previous location
      let delta = this.sprite.position.clone().sub(this.origin);
      let dist = delta.length();
      delta.normalize();

      let angle = (this.angle - this.sprite.angle) % TWOPI;
      if (Math.abs(angle) > Math.PI) {
        if (angle > 0) {
          angle = (angle - TWOPI);
        } else {
          angle = (angle + TWOPI);
        }
      }

      if (angle > 0) {
        angle = Math.min(angle, this.data.spring);
      } else {
        angle = -Math.min(-angle, this.data.spring);
      }

      this.sprite.body.impulse(
        {x: 0, y: 0}, //delta.multiplyScalar(-Math.min(this.data.spring, dist)),
        angle
      );
    }
  }

  destroy() {
    this.game.world.destroyJoint(this.sprite);
    this.sprite.joint = null;
  }
}
