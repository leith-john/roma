import Behavior from "./base";
import Controller from "./controller";


/**
 * The controller creates a cursor and this object tries to go there
 */
export default class MoveToCursor extends Controller {


  constructor(game, sprite, data) {
    super(game, sprite, data);
    this.cooldown = 0;
  }

  turn(amount) {
    let sign = amount < 0 ? -1 : 1;
    this.game.world.impulseBody({
      id: this.sprite.body.id,
      torque: sign * Math.min(this.data.turn_rate, Math.abs(amount))
    });
  }

  accelerate(scale) {
    let delta = this.forward.multiplyScalar(this.data.speed * scale);
    this.game.world.impulseBody({
      id: this.sprite.body.id,
      force: {x: delta.x, y: delta.y},
    });
  }

  update() {
    if (this.sprite.states.cursor &&
        this.sprite.states.go &&
        this.sprite.states.cursor.distanceTo(this.sprite.position) > 0.01) {

      if (this.cooldown <= 0) {
        let delta = this.sprite.states.cursor.clone().sub(this.sprite.position);
        let magnitude = delta.length();
        delta.normalize();

        delta = this.sprite.position.clone().add(
          delta.multiplyScalar(Math.min(magnitude, this.data.speed))
        );
        // let torque = this.turnTo(this.sprite.states.cursor);

        this.game.world.impulseBody({
          id: this.sprite.body.id,
          torque: -this.sprite.body.angularVelocity
        });

        this.game.world.updateBody({
          id: this.sprite.body.id,
          angle: 0,
          x: delta.x,
          y: delta.y,
          // torque: torque
        });

        this.cooldown += 0;
      } else {
        this.cooldown -= 1;
      }
    }
  }
}
