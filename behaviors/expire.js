/* globals window THREE */

import Behavior from "./base";

export default class ExpireBehavior extends Behavior {

  get defaults() {
    return {
      duration: 1000
    };
  }
  constructor(game, sprite, data) {
    super(game, sprite, data);
    this.start = game.time;
  }

  update() {
    if (this.game.time - this.start > this.data.duration) {
      this.sprite.alive = false;
      return false;
    }
  }
}
