import Behavior from "./base";

export default class DeathSpawnBehavior extends Behavior {
  update() {
    // nothing
  }

  destroy() {
    if (!this.sprite.states["despawn"]) {
      this.game.load([
        {
          type: "sprite",
          extends: this.data.blueprint,
          position: this.sprite.position.clone(),
        },
      ]);
    }
  }
}
