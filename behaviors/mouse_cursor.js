/* globals document */
import Behavior from "./base";


export default class MouseCursor extends Behavior {

  async load() {
    this._listener = this.listener.bind(this);
    document.addEventListener("mousemove", this._listener, true);
    document.addEventListener("mouseup", this._listener, true);
    document.addEventListener("mousedown", this._listener, true);
    document.addEventListener("contextmenu", (e) => e.preventDefault(), true);
  }

  listener(e) {
    if (e.type != "mousemove") {
      e.preventDefault();
      this._ready = Date.now();
    }

    if (this.sprite) {
      this.sprite.states.mouse = {x: e.clientX, y: e.clientY};
      if (e.type == "mousedown") {
        this.sprite.states["button" + e.button] = true;
      }
      if (e.type == "mouseup") {
        this.sprite.states["button" + e.button] = false;
      }
    }
  }

  destroy() {
    super.destroy();
    document.removeEventListener("mousemove", this._listener);
    document.removeEventListener("mouseup", this._listener);
    document.removeEventListener("mousedown", this._listener);
  }

  update() {
    if (this.sprite.states.mouse) {
      let mouse = this.sprite.states.mouse;
      let point = new THREE.Vector3(
        (mouse.x / this.game.width) * 2 - 1,
        - (mouse.y / this.game.height) * 2 + 1,
        5
      ).unproject( this.game.camera );

      // direction to point from camera
      let dir = point.sub(this.game.camera.position).normalize();
      let distance = - (this.game.camera.position.z) / dir.z;

      point = this.game.camera.position.clone().add(
        dir.multiplyScalar(distance)
      );
      this.sprite.states.cursor = point;
    }
    this.sprite.states.go = this.sprite.states.button0;
  }
}
