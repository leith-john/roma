import Behavior from "./base";

export default class HealthBehavior extends Behavior {

  get defaults() {
    return {
      amount: 100
    };
  }

  constructor(game, sprite, data) {
    super(game, sprite, data);
    this.sprite.hp = this.data.amount
  }

  update() {
    this.sprite.alive = this.sprite.hp > 0;
  }
}
