import Behavior from "../base";

export default class JumpBehavior extends Behavior {

  get defaults() {
    return {
      jump: 0,
      max: 0.7,
      cooldown: 0.05,
      power: 5,
    };
  }

  constructor(game, sprite, data) {
    super(game, sprite, data);
    this.tired = false;
  }

  update() {
    if (this.sprite.states.vector) {
      let dz = this.sprite.body.velocity.z;
      let pz = this.sprite.states.vector.z;

      // how high are they jumping?
      // if (dz > 0) {
      //   if (dz < this.last && ! this.down) {
      //     console.log(dz);
      //     this.down = true;
      //   } else {
      //     this.last = dz;
      //     this.down = false;
      //   }
      // }

      if (pz != 0 && ! this.tired && this.sprite.body.contacts > 0) {
        if (this.data.jump < this.data.max) {
          pz = Math.min(this.data.max - this.data.jump, pz);
        } else {
          pz = 0;
          this.tired = true;
        }
        this.data.jump += pz;
        if (pz > 0) {
          this.game.world.impulseBody({
            id: this.sprite.body.id,
            force: {x: 0, y: 0, z: pz * this.data.power}
          });
        }
      } else if (this.data.jump >= this.data.cooldown) {
        this.data.jump -= this.data.cooldown;
      } else {
        this.data.jump = 0;
        this.tired = false;
      }
    }
  }
}
